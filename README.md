# Praca inżynierska
## Uniwersytet Rzeszowski 2018
## Sadok Patryk
## Nr. alb.: 092093
## Informatyka, spec. Aplikacje Internetowe

# Aplikacja do zaprządzania starostwem powiatowym


### Instrukcja użytkowania:
## Wymagania uruchomieniowe:
1. Zainstalowany Docker, Node.js, npm.
2. Zalecane zainstalowanie Yarn (można używać zamiennie z npm).
3. Terminal komend linux/macOS (odczytywanie skryptów .sh oraz Dockerfile).
## Uruchamianie projektu:
1. Będąc w folderze głównym (praca-inzynierska):
2. Uruchomienie komendy ```./docker_build_db.sh ```.
3. Uruchomienie komendy ```./docker_up_db.sh ```.
4. Przejście do folderu backend.
5. Uruchomienie komendy ```yarn install ```.
6. Uruchomienie komendy ```./node_modules/.bin/sequelize db:migrate:undo:all ``` - usunięcie istniejących migracji z bazy.
7. Uruchomienie komendy ```./node_modules/.bin/sequelize db:migrate ``` - dodanie migracji do bazy.
8. Uruchomienie komendy ```./node_modules/.bin/sequelize db:seed:all ``` - dodanie stałych do bazy (role, sekcje etc)
9. Uruchomienie komendy ```node app.js ```.
10. W drugim oknie terminalu, przejście do folderu frontend.
11. Uruchomienie komendy ```yarn install```.
12. Uruchomienie komendy ```yarn start```.


## Przydatne komendy:
1. ```docker exec -it test_task bash``` - wejście w terminal bash dockera bazy danych

## Linki:
1. Trello: ```https://trello.com/b/2JCc9oNv/praca-in%C5%BCynierska```

dokumentacja:
1. wprowadzenie i cel pracy //o systemach informatycznych wprowadzających pracę urzędów i powód tworzenia systemu
2. opisać podobny system (asseco)
3. użyte narzędzia, technologie, frameworki
4. projekt systemu (UML, przypadki użycia, przepływu, stanu), architektura
5. implementacja //opisanie fragmentów kodu, prezentacja aplikacji
6. scenariusz użycia (opis możliwości)
7. podsumowanie //czy założenia zostały zrealizowane, kierunki w przyszłości


https://flaviocopes.com/express-send-files/
https://hk.saowen.com/a/fd2bd4bdd01df0c9c9d13b12ba764093d86fec7f421df97e3654d861d02751a5
https://flaviocopes.com/tags/express/
render do pobrnaia pliku