import React from 'react';
import {Route} from 'react-router-dom';
import Home from '../sites/home/index';
import About from '../sites/about/about';
import EditAddress from '../sites/editAddress';
import EditProfile from '../sites/editProfile';
import Profile from '../sites/user/profile';
import UserCaseView from '../sites/user/case';
import UserCasesList from '../sites/user/caseList';
import CreateCase from '../sites/user/createCase';
import ChooseContext from '../sites/chooseContext';
import OfficialCasesList from '../sites/official/cases';
import OfficialProfile from '../sites/official/profile';
import OfficialCaseView from '../sites/official/case';
import ManagerProfile from '../sites/manager/profile';
import ManagerCasesList from '../sites/manager/cases';
import ManagerCaseView from '../sites/manager/case';
import AdminUserList from '../sites/admin/userList';

export const routes = [
    {
        component: Home,
        exact: true,
        path: '/'
    },
    {
        component: About,
        path: '/about'
    },
    {
        component: EditAddress,
        path: '/edit-address'
    },
    {
        component: Profile,
        path: '/profile'
    },
    {
        component: OfficialProfile,
        path: '/profile-official'
    },
    {
        component: EditProfile,
        path: '/edit-profile'
    },
    {
        component: UserCaseView,
        path:'/case/:id'
    },
    {
        component: OfficialCaseView,
        path:'/case-official/:id'
    },
    {
        component: UserCasesList,
        path:'/usercases'
    },
    {
        component: CreateCase,
        path: '/create-case'
    },
    {
        component: ChooseContext,
        path:'/choose-context'
    },
    {
        component: OfficialCasesList,
        path:'/official-cases'
    },
    {
        component: ManagerProfile,
        path:'/profile-manager'
    },
    {
        component: ManagerCasesList,
        path:'/manager-cases'
    },
    {
        component: ManagerCaseView,
        path:'/case-manager/:id'
    },
    {
        component: AdminUserList,
        path:'/user-list'
    }
];

export const RouteWithSubRoutes = (route, key) => (
    <Route
        exact={route.exact}
        key={key}
        path={route.path}
        render={(props) => (
            <route.component
                {...props}
                routes={route.routes}
            />
        )}
    />
);
