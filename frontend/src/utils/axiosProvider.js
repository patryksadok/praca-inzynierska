import ax from 'axios';
const devAPI = 'localhost:3003';

const axios = ax.create();

axios.interceptors.request.use((axios) => {
    if (localStorage.getItem('accessToken')) {
        axios.headers = {Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
            'Content-Type': 'application/json'};
    }
    if (['development'].includes(process.env.NODE_ENV)) {
        axios.url = `http://${devAPI}${axios.url}`;
    } else if ([
        'staging',
        'production'
    ].includes(process.env.NODE_ENV)) {
        axios.url = `https://${prodAPI}${axios.url}`;
    }
    return axios;
});

export default axios;
