import React, {Fragment} from 'react';
import Header from './header';
import Footer from './footer';
import '../style/index.scss';
import { Grid, Row, Col} from "react-bootstrap";

const Index = ({children}) => {
    return (
        <div className="Site">
            <Header />
            <Fragment>
                <Grid fluid>
                    <Row>
                        <Col
                            lg={12}
                            md={12}
                            sm={12}
                            xs={12}
                        >
                            {children}
                        </Col>
                    </Row>
                </Grid>
            </Fragment>
            <Footer />
        </div>
    );
};

export default Index;
