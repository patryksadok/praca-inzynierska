import {connect} from 'react-redux';
import Container from './officialSet';

const mapStateToProps = (state) => {
    return {
        accessToken: state.sessionReducer.accessToken
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
