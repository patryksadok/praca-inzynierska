import React, {Component, Fragment} from "react";
import {Nav, NavItem} from "react-bootstrap";
import {withRouter} from 'react-router-dom';

class NavBar extends Component {
    handleRedirect = (url='') => {
        const {history} = this.props;
        history.push(`/${url}`);
    }

    render() {
        const {activeContext, handleLogout, accessToken} = this.props;
        return (
            <Fragment>
                {accessToken &&
                    <Nav pullRight>
                        <NavItem onClick={() => {this.handleRedirect('choose-context')}}>
                            Kontekst:  {activeContext}
                        </NavItem>
                        <NavItem onClick={() => {this.handleRedirect('official-cases')}}>
                            {"Sprawy"}
                        </NavItem>
                        <NavItem onClick={() => {this.handleRedirect('profile-official')}}>
                            {"Profil"}
                        </NavItem>
                        <NavItem onClick={handleLogout}>
                            {'Wyloguj'}
                        </NavItem>
                    </Nav>
                }
            </Fragment>
        );
    }
}

export default withRouter(NavBar);
