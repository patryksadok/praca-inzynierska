import React, {Component} from "react";
import {Navbar, Nav, NavItem} from "react-bootstrap";
import {withRouter} from 'react-router-dom';
import LoginModal from '../../../src/components/modals/login';
import UserSet from './linkSets/userSet';
import OfficialSet from './linkSets/officialSet';
import ManagerSet from './linkSets/managerSet';
import AdminSet from './linkSets/adminSet';

class NavBar extends Component {
    state = {
        aboutPageLink: "O projekcie",
        loginButton: "Zaloguj",
        loginModalShow: false,
        logoText: "Starostwo powiatowe"
    };

    componentDidMount() {
        if (localStorage.getItem('accessToken')) {
            const {getContext} = this.props;
            getContext();
        }
    }

    handleSessionClick = () => {
        const {history} = this.props;
        history.push('/');
    }

    handleRedirectToHome = () => {
        const {history} = this.props;
        history.push('/');
    }

    handleRedirectToAbout = () => {
        const {history} = this.props;
        history.push('/about');
    }

    handleRedirectToProfile = () => {
        const {history} = this.props;
        history.push('/profile');
    }

    handleRedirectToCases = () => {
        const {history} = this.props;
        history.push('/usercases');
    }

    handleRedirectToContext = () => {
        const {history} = this.props;
        history.push('/choose-context');
    }

    handleInvokeLoginModal = () => {
        this.setState({
            loginModalShow: true
        });
    }

    onLogout = async () => {
        const {logoutUser} = this.props;
        await logoutUser();
        window.location.href = "/";
    }

    render() {
        const loginModalClose = () => this.setState({loginModalShow: false});
        const {logoText, loginButton, aboutPageLink, loginModalShow} = this.state;
        const {activeContext} = this.props;

        return (
            <div className="main-navbar">
                <Navbar
                    bStyle="custom"
                    collapseOnSelect
                    fluid
                    inverse
                    staticTop
                >
                    <Navbar.Header>
                        <Navbar.Brand onClick={this.handleRedirectToHome}>
                            {logoText}
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            <NavItem onClick={this.handleRedirectToAbout}>
                                {aboutPageLink}
                            </NavItem>
                        </Nav>
                        {!localStorage.getItem('accessToken') &&
                        <Nav pullRight>
                            <NavItem onClick={this.handleInvokeLoginModal}>
                                {loginButton}
                            </NavItem>
                        </Nav>
                        }
                        {activeContext === 'Użytkownik' &&
                            <UserSet
                                activeContext={activeContext}
                                handleLogout={this.onLogout}
                            />
                        }
                        {activeContext === 'Urzędnik' &&
                            <OfficialSet
                                activeContext={activeContext}
                                handleLogout={this.onLogout}
                            />
                        }
                        {activeContext === 'Kierownik' &&
                            <ManagerSet
                                activeContext={activeContext}
                                handleLogout={this.onLogout}
                            />
                        }
                        {activeContext === 'Admin' &&
                            <AdminSet
                                activeContext={activeContext}
                                handleLogout={this.onLogout}
                            />
                        }
                    </Navbar.Collapse>
                </Navbar>

                <LoginModal
                    onHide={loginModalClose}
                    show={loginModalShow}
                />
            </div>
        );
    }
}

export default withRouter(NavBar);
