import {connect} from 'react-redux';
import Header from './header';
import {logoutUser} from '../../actions/sessionActions';
import {getContext} from '../../actions/contextActions';

const mapStateToProps = (state) => {
    const {accessToken} = state.sessionReducer;
    const {activeRole} = state.contextReducer.activeContext;
    return {
        activeRole,
        accessToken,
        activeContext: state.contextReducer.activeContext.activeRole
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getContext: () => dispatch(getContext()),
        logoutUser: () => dispatch(logoutUser())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
