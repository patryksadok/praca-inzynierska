import React, { Component } from "react";
import { Navbar} from "react-bootstrap";
import { withRouter } from 'react-router-dom';

class NavBar extends Component {
    state = {
        logoText: "2018/2019 Patryk Sadok - Informatyka IV Uniwersytet Rzeszowski"
    };

    handleRedirectToHome = () => {
        const {history} = this.props;
        history.push('/');
    }

    render() {
        const { logoText } = this.state;
        return (
            <div className="footer">
                <Navbar
                    collapseOnSelect
                    fixedBottom
                    fluid
                    inverse
                >
                    <Navbar.Header>
                        <Navbar.Brand onClick={this.handleRedirectToHome}>
                            {logoText}
                        </Navbar.Brand>
                    </Navbar.Header>
                </Navbar>
            </div>
        );
    }
}

export default withRouter(NavBar);
