/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, FormGroup, ControlLabel, FormControl} from "react-bootstrap";
import Loader from "../../components/shared/loader";

export default class EditAddress extends Component {
    componentDidMount() {
        const {getContextList} = this.props;
        getContextList();
    };

    handleContextChange = async (event) => {
        const {setContext, history} = this.props;
        await setContext(event.target.value);
        history.push('/');
    };

    render() {
        const {contextList} = this.props;

        if (!contextList.profiles) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {'Wybieranie kontekstu'}
                    </h2>
                    <h4>
                        {'Kontekst to rola systemowa. Pozwala przełączać się pomiędzy dostępnymi profilami użytkowników oraz pracowników systemu.'}
                    </h4>
                    <h4>
                        {'Po wybraniu kontekstu zostaniesz przekierowany na stronę główną.'}
                    </h4>
                    <form>
                        <FormGroup controlId="formControlsSelect">
                            <ControlLabel>{'Wybierz kontekst'}</ControlLabel>
                            <FormControl componentClass="select" placeholder="select" onChange={this.handleContextChange} defaultValue={contextList.activeProfile} >
                                {
                                    contextList.profiles.map((item, iterator) => {
                                        return (
                                            <option key={item.profile.id} value={item.profile.id}>{item.roleName}</option>
                                        );
                                    })
                                }
                            </FormControl>
                        </FormGroup>
                    </form>
                </Jumbotron>
            </div>
        );
    }
}
