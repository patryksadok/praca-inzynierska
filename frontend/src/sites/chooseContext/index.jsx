import {connect} from 'react-redux';
import Container from './chooseContext';
import {
    getContextList,
    setContext
} from '../../actions/contextActions';

const mapStateToProps = (state) => {
    return {
        contextList: state.contextReducer.contextList,
        activeContext: state.contextReducer.activeContext.activeRole
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getContextList: () => dispatch(getContextList()),
        setContext: (contextId) => dispatch(setContext(contextId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
