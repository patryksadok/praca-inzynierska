import {connect} from 'react-redux';
import Container from './editAddress';
import {getAddresses} from '../../actions/addressActions';

const mapStateToProps = (state) => {
    return {
        checkedAddress: state.addressReducer.checkedAddress,
        correspondenceAddress: state.addressReducer.correspondenceAddress
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAddresses: () => dispatch(getAddresses())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
