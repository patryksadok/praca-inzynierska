/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, Checkbox} from "react-bootstrap";
import AddressForm from '../../components/user/addressForm';
import Loader from "../../components/shared/loader";

export default class EditAddress extends Component {
    state = {
        bothAddresses: "Adres zameldowania i korespondencji",
        checkboxMessage: "Zaznacz, jeśli adresy korespodencyjny i zameldowania są te same.",
        firstAddress: "Adres korespondencyjny",
        renderSecondForm: true,
        secondAddress: "Adres zameldowania",
        welcomeMessage: "Edycja adresu"
    };

    componentDidMount() {
        const {getAddresses} = this.props;
        getAddresses();
    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    handleCheckboxChange = () => {
        const {renderSecondForm} = this.state;
        this.setState({renderSecondForm: !renderSecondForm});
    };

    handleRedirectToProfile = () => {
        const {history} = this.props;
        history.push('/profile');
    };

    render() {
        const {welcomeMessage, checkboxMessage, renderSecondForm, firstAddress, secondAddress, bothAddresses} = this.state;
        const {checkedAddress, correspondenceAddress} = this.props;
        if (!correspondenceAddress || !checkedAddress) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {welcomeMessage}
                    </h2>
                    <h4 onClick={this.handleRedirectToProfile}>
                        {"Powrót do profilu"}
                    </h4>
                    <Checkbox
                        checked={!renderSecondForm}
                        onChange={this.handleCheckboxChange}
                    >
                        {checkboxMessage}
                    </Checkbox>
                    {!renderSecondForm &&
                        <AddressForm
                            addressType="both"
                            data={correspondenceAddress}
                            header={bothAddresses}
                        />
                    }
                    {renderSecondForm &&
                    <div>
                        <AddressForm
                            addressType="correspondence"
                            data={correspondenceAddress}
                            header={firstAddress}
                        />
                        <AddressForm
                            addressType="check"
                            data={checkedAddress}
                            header={secondAddress}
                        />
                    </div>
                    }
                </Jumbotron>
            </div>
        );
    }
}
