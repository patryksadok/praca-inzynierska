/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron }  from "react-bootstrap";
import CasesList from '../../../components/official/casesList';

export default class Profile extends Component {
    render() {
       
        return (
            <div>
                <Jumbotron>
                    <h2>Podgląd spraw:</h2>
                </Jumbotron>
                <Jumbotron>
                    <CasesList />
                </Jumbotron>
            </div>
        );
    }
}
