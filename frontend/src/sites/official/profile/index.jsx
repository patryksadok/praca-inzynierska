import {connect} from 'react-redux';
import Container from './officialProfile';
import {getOfficialProfile} from '../../../actions/profileActions';

const mapStateToProps = (state) => {
    return {
        profileData: state.profileReducer.officialProfile
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getOfficialProfile: () => dispatch(getOfficialProfile())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
