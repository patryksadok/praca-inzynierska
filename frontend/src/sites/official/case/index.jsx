import {connect} from 'react-redux';
import Container from './case';
import {getOfficialCaseDetails, closeCase, confirmCase} from '../../../actions/caseActions';
import {addComment} from '../../../actions/commentActions';
import {uploadAttachment} from '../../../actions/fileActions';

const mapStateToProps = (state) => {
    return {
        officialCase: state.caseReducer.officialCase
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getOfficialCaseDetails: (id) => dispatch(getOfficialCaseDetails(id)),
        closeCase: (id) => dispatch(closeCase(id)),
        confirmCase: (id) => dispatch(confirmCase(id)),
        addComment: (id, comment) => dispatch(addComment(id, comment)),
        uploadAttachment: (file, id) => dispatch(uploadAttachment(file, id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
