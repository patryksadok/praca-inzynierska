/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import {Jumbotron} from "react-bootstrap";
import Loader from "../../../components/shared/loader";
import AttachmentsContainer from '../../../components/attachments/container';
import CommentsContainer from '../../../components/comments/container';
import ConfirmationModal from '../../../components/shared/confirmationModal';
import CommentModal from '../../../components/shared/textInputModal';
import AttachmentModal from '../../../components/shared/uploadFileModal';
import moment from 'moment';

export default class EditProfile extends Component {
    state = {
        deletionModal: false,
        acceptingModal: false,
        commentModal: false,
        attachmentModal: false,
        attachmentModalHeaderText: "Dodawanie załącznika",
        commentModalHeaderText: "Dodawanie komentarza",
        commentModalBodyText: "Wpisz treść komentarza",
        acceptingModalHeaderText: "Potwierdzenie akceptacji sprawy",
        acceptingModalBodyText: "Potwierdź akceptację sprawy",
        deletionModalHeaderText: "Potwierdzenie zamknięcia sprawy",
        deletionModalBodyText: "Czy na pewno chcesz zamknąć tę sprawę? Tej akcji nie można odwrócić."
    };

    componentDidMount() {
        const {match: {params: {id}}, getOfficialCaseDetails} = this.props;
        getOfficialCaseDetails(id);
    }

    handleCloseModals = () => {
        this.setState({deletionModal: false, acceptingModal: false, commentModal: false, attachmentModal: false});
    };

    closeCase = () => {
        const {match: {params: {id}}, closeCase, history} = this.props;
        this.setState({deletionModal: false});
        closeCase(id);
        history.push('/official-cases');
    }

    acceptCase = () => {
        const {match: {params: {id}}, confirmCase, history} = this.props;
        this.setState({deletionModal: false});
        confirmCase(id);
        history.push('/official-cases');
    }

    addComment = async (comment) => {
        const {match: {params: {id}}, addComment, getOfficialCaseDetails} = this.props;
        await addComment(id, comment);
        await this.setState({commentModal: false});
        await getOfficialCaseDetails(id);
    }

    addAttachment = async (file) => {
        const {uploadAttachment, getOfficialCaseDetails} = this.props;
        const {match: {params: {id}}} = this.props;
        await uploadAttachment(file, id);
        await this.setState({attachmentModal: false});
        await getOfficialCaseDetails(id);
    };

    openDeletionModal = () => {
        this.setState({deletionModal: true});
    };

    openAcceptingModal = () => {
        this.setState({acceptingModal: true});
    };

    openCommentModal = () => {
        this.setState({commentModal: true});
    };

    openAttachmentModal = () => {
        this.setState({attachmentModal: true});
    };

    render() {
        const {match: {params: {id}}, officialCase} = this.props;
        const {deletionModal, deletionModalBodyText, deletionModalHeaderText,
            acceptingModal, acceptingModalBodyText, acceptingModalHeaderText,
            commentModal, commentModalBodyText, commentModalHeaderText,
            attachmentModal, attachmentModalHeaderText} = this.state;
        const minimumLength = 0;

        if (!officialCase) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500}
                    type="Watch"
                    width={500}
                />
            );
        }
        const updatedAt = moment(officialCase.caseData.caseDetails.updatedAt).format("DD/MM/YYYY");
        const createdAt = moment(officialCase.caseData.caseDetails.createdAt).format("DD/MM/YYYY");

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {officialCase.caseData.caseDetails.description}
                    </h2>
                    <h4>
                        {`Numer sprawy: ${id}`}
                    </h4>
                    <h4>
                        {`Data utworzenia sprawy: ${createdAt}`}
                    </h4>
                    <h4>
                        {`Ostatnia aktualizacja: ${updatedAt}`}
                    </h4>
                    <h4>
                        {`Status sprawy: ${officialCase.caseStatus}`}
                    </h4>
                    {officialCase.caseData.caseDetails.closedAt &&
                        <h4>{`Data zamknięcia: ${moment(officialCase.caseData.caseDetails.closedAt).format("DD/MM/YYYY")}`}</h4>
                    }
                    {officialCase.caseData.clientNames.names &&
                        <h4>
                            {`Prowadzący sprawę: ${officialCase.caseData.clientNames.names.firstName} ${officialCase.caseData.clientNames.names.lastName}`}
                        </h4>
                    }
                    {officialCase.caseData.caseDetails.verdictId &&
                        <h4>
                            {`Werdykt: ${id}`}
                        </h4>
                    }
                </Jumbotron>
                <Jumbotron>
                    {officialCase.caseStatus === 'Oczekująca' && 
                        <h4 onClick={this.openAcceptingModal}>Zatwierdź sprawę</h4>
                    }
                    {officialCase.caseStatus !== 'Zamknięta' && 
                        <h4 onClick={this.openDeletionModal}>Zamknij sprawę</h4>
                    }
                    {officialCase.caseStatus === 'W toku' && 
                        <div>
                            <h4 onClick={this.openAttachmentModal}>Dodaj załącznik</h4>
                            <h4 onClick={this.openCommentModal}>Dodaj komentarz</h4>
                        </div>
                    }
                </Jumbotron>
                {officialCase.caseData.attachmentsArray.length > minimumLength &&
                    <AttachmentsContainer data={officialCase.caseData.attachmentsArray} />
                }
                {officialCase.caseData.comments.length > minimumLength &&
                    <CommentsContainer data={officialCase.caseData.comments} />
                }
                <ConfirmationModal opened={deletionModal} handleClose={this.handleCloseModals} handleConfirm={this.closeCase}
                    headerText={deletionModalHeaderText} bodyText={deletionModalBodyText} />
                <ConfirmationModal opened={acceptingModal} handleClose={this.handleCloseModals} handleConfirm={this.acceptCase}
                    headerText={acceptingModalHeaderText} bodyText={acceptingModalBodyText} />
                <CommentModal opened={commentModal} handleClose={this.handleCloseModals} handleConfirm={this.addComment}
                    headerText={commentModalHeaderText} bodyText={commentModalBodyText} />
                <AttachmentModal opened={attachmentModal} handleClose={this.handleCloseModals} handleConfirm={this.addAttachment}
                    headerText={attachmentModalHeaderText} />
            </div>
        );
    }
}
