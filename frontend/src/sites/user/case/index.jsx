import {connect} from 'react-redux';
import Container from './case';
import {getUserCaseDetails, closeCase} from '../../../actions/caseActions';
import {addComment} from '../../../actions/commentActions';
import {uploadAttachment} from '../../../actions/fileActions';

const mapStateToProps = (state) => {
    return {
        userCase: state.caseReducer.userCase
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserCaseDetails: (id) => dispatch(getUserCaseDetails(id)),
        closeCase: (id) => dispatch(closeCase(id)),
        addComment: (id, comment) => dispatch(addComment(id, comment)),
        uploadAttachment: (file, id) => dispatch(uploadAttachment(file, id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
