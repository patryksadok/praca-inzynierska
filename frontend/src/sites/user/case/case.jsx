/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import {Jumbotron} from "react-bootstrap";
import Loader from "../../../components/shared/loader";
import AttachmentsContainer from '../../../components/attachments/container';
import CommentsContainer from '../../../components/comments/container';
import ConfirmationModal from '../../../components/shared/confirmationModal';
import CommentModal from '../../../components/shared/textInputModal';
import AttachmentModal from '../../../components/shared/uploadFileModal';
import moment from 'moment';

export default class EditProfile extends Component {
    state = {
        deletionModal: false,
        attachmentModal: false,
        commentModal: false,
        attachmentModalHeaderText: "Dodawanie załącznika",
        commentModalHeaderText: "Dodawanie komentarza",
        commentModalBodyText: "Wpisz treść komentarza",
        modalHeaderText: "Potwierdzenie zamknięcia sprawy",
        modalBodyText: "Czy na pewno chcesz zamknąć tę sprawę? Tej akcji nie można odwrócić."
    };

    componentDidMount() {
        const {match: {params: {id}}, getUserCaseDetails} = this.props;
        getUserCaseDetails(id);
    }

    handleCloseModals = () => {
        this.setState({deletionModal: false, commentModal: false, attachmentModal: false})
    };

    addAttachment = (file) => {
        const {uploadAttachment} = this.props;
        const {match: {params: {id}}} = this.props;
        uploadAttachment(file, id);
        this.setState({attachmentModal: false});
    };

    closeCase = () => {
        const {match: {params: {id}}, closeCase, history} = this.props;
        this.setState({deletionModal: false});
        closeCase(id);
        history.push('/profile');
    }

    addComment = async (comment) => {
        const {match: {params: {id}}, addComment, getUserCaseDetails} = this.props;
        await addComment(id, comment);
        await this.setState({commentModal: false});
        await getUserCaseDetails(id);
    }

    openCommentModal = () => {
        this.setState({commentModal: true});
    };

    openDeletionModal = () => {
        this.setState({deletionModal: true})
    };

    openAttachmentModal = () => {
        this.setState({attachmentModal: true});
    };

    render() {
        const {match: {params: {id}}, userCase} = this.props;
        const {deletionModal, modalBodyText, modalHeaderText,
            commentModal, commentModalBodyText, commentModalHeaderText,
            attachmentModal, attachmentModalHeaderText} = this.state;
        const minimumLength = 0;

        if (!userCase) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500}
                    type="Watch"
                    width={500}
                />
            );
        }
        const updatedAt = moment(userCase.caseData.caseDetails.updatedAt).format("DD/MM/YYYY");
        const createdAt = moment(userCase.caseData.caseDetails.createdAt).format("DD/MM/YYYY");

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {userCase.caseData.caseDetails.description}
                    </h2>
                    <h4>
                        {`Numer sprawy: ${id}`}
                    </h4>
                    <h4>
                        {`Data utworzenia sprawy: ${createdAt}`}
                    </h4>
                    <h4>
                        {`Ostatnia aktualizacja: ${updatedAt}`}
                    </h4>
                    <h4>
                        {`Status sprawy: ${userCase.caseStatus}`}
                    </h4>
                    {userCase.caseData.caseDetails.closedAt &&
                        <h4>{`Data zamknięcia: ${moment(userCase.caseData.caseDetails.closedAt).format("DD/MM/YYYY")}`}</h4>
                    }
                    {userCase.caseData.officialNames.names &&
                        <h4>
                            {`Prowadzący sprawę: ${userCase.caseData.officialNames.names.firstName} ${userCase.caseData.officialNames.names.lastName}`}
                        </h4>
                    }
                    {userCase.caseData.caseDetails.verdictId &&
                        <h4>
                            {`Werdykt: ${id}`}
                        </h4>
                    }
                </Jumbotron>
                <Jumbotron>
                    {userCase.caseStatus !== 'Zamknięta' && 
                        <h4 onClick={this.openDeletionModal}>Zamknij sprawę</h4>
                    }
                    <h4 onClick={this.openAttachmentModal}>Dodaj załącznik</h4>
                    <h4 onClick={this.openCommentModal}>Dodaj komentarz</h4>
                </Jumbotron>
                {userCase.caseData.attachmentsArray.length > minimumLength &&
                    <AttachmentsContainer data={userCase.caseData.attachmentsArray} />
                }
                {userCase.caseData.comments.length > minimumLength &&
                    <CommentsContainer data={userCase.caseData.comments} />
                }
                <ConfirmationModal opened={deletionModal} handleClose={this.handleCloseModals} handleConfirm={this.closeCase}
                    headerText={modalHeaderText} bodyText={modalBodyText} />
                <CommentModal opened={commentModal} handleClose={this.handleCloseModals} handleConfirm={this.addComment}
                    headerText={commentModalHeaderText} bodyText={commentModalBodyText} />
                <AttachmentModal opened={attachmentModal} handleClose={this.handleCloseModals} handleConfirm={this.addAttachment}
                    headerText={attachmentModalHeaderText} />
            </div>
        );
    }
}
