import {connect} from 'react-redux';
import Container from './profile';
import {getAddresses} from '../../../actions/addressActions';
import {getProfile} from '../../../actions/profileActions';
import {getUserCasesList} from '../../../actions/caseActions';

const mapStateToProps = (state) => {
    return {
        checkedAddress: state.addressReducer.checkedAddress,
        correspondenceAddress: state.addressReducer.correspondenceAddress,
        profileData: state.profileReducer.profile,
        casesList: state.caseReducer.userCasesList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAddresses: () => dispatch(getAddresses()),
        getProfile: () => dispatch(getProfile()),
        getUserCasesList: () => dispatch(getUserCasesList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
