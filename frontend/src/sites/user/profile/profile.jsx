/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, Tabs, Tab }  from "react-bootstrap";
import AddressSection from "../../../components/user/addressSection";
import ProfileSection from "../../../components/user/profileSection";
import CasesList from "../../../components/user/casesList";
import Loader from "../../../components/shared/loader";
import { Link } from 'react-router-dom';

export default class Profile extends Component {
    state = {
        welcomeMessage: "Profil użytkownika"
    };

    async componentDidMount() {
        const {getAddresses, getProfile, getUserCasesList} = this.props;
        await getAddresses();
        await getProfile();
        await getUserCasesList();
    };

    render() {
        const {welcomeMessage} = this.state;
        const {correspondenceAddress, checkedAddress, profileData, casesList} = this.props;

        if (!correspondenceAddress || !checkedAddress || !profileData || !casesList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }
        
        return (
            <div>
                <Jumbotron>
                    <h2>
                        {welcomeMessage}
                    </h2>
                </Jumbotron>
                <Tabs defaultActiveKey={1}>
                    <Tab
                        eventKey={1}
                        title="Dane profilu"
                    >
                        <ProfileSection
                            data={profileData}
                            header="Dane osobowe"
                        />
                    </Tab>
                    <Tab
                        eventKey={2}
                        title="Moje sprawy"
                    >
                        <Jumbotron>
                            <CasesList data={casesList} />
                        </Jumbotron>
                    </Tab>
                    <Tab
                        eventKey={3}
                        title="Moje adresy"
                    >
                        <Jumbotron>
                            <h3>
                                {"Adresy"}
                            </h3>
                            <Link to="/edit-address">
                                <h4>
                                    {"Edytuj adresy"}
                                </h4>
                            </Link>
                        </Jumbotron>
                        <AddressSection
                            data={correspondenceAddress}
                            header="Adres korespondencyjny"
                        />
                        <AddressSection
                            data={checkedAddress}
                            header="Adres zameldowania"
                        />
                    </Tab>
                </Tabs>
            </div>
        );
    }
}
