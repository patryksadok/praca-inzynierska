/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, Button }  from "react-bootstrap";
import CasesList from "../../../components/user/casesList";
import Loader from "../../../components/shared/loader";

export default class Profile extends Component {
    state = {
    };

    componentDidMount() {
        const {getUserCasesList} = this.props;
        getUserCasesList();
    }; 

    handleCreateCaseClick = () => {
        const {history} = this.props;
        history.push('/create-case');
    };

    render() {
        const {casesList} = this.props;

        if (!casesList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }
        
        return (
            <div>
                <Jumbotron>
                    <h4>Menu spraw:</h4>
                    <Button bsStyle="primary" onClick={this.handleCreateCaseClick}>Załóż nową sprawę</Button>
                </Jumbotron>
                <Jumbotron>
                    <CasesList data={casesList} />
                </Jumbotron>
            </div>
        );
    }
}
