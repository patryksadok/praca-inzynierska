import {connect} from 'react-redux';
import Container from './caseList';
import {getUserCasesList} from '../../../actions/caseActions';

const mapStateToProps = (state) => {
    return {
        casesList: state.caseReducer.userCasesList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserCasesList: () => dispatch(getUserCasesList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
