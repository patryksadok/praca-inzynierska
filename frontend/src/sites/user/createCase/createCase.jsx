/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, Button, FormGroup, ControlLabel, FormControl, HelpBlock }  from "react-bootstrap";

export default class Home extends Component {
    state = {
        description: ''
    };

    handleCreateCaseClick = async () => {
        const {history, createCase} = this.props;
        const {description} = this.state;
        await createCase(description);
        await history.push('/usercases');
    };

    handleChange = (ev) => {
        this.setState({ description: ev.target.value });
    };
    
    getValidationState = () => {
        const length = this.state.description.length;
        if (length > 5) return 'success';
        else if (length > 0) return 'error';
        return null;
    };
    

    render() {
        return (
            <div>
               <Jumbotron>
                    <h2>Tworzenie nowej sprawy</h2>
                    <form>
                        <FormGroup
                            controlId="formBasicText"
                            validationState={this.getValidationState()}
                        >
                            <ControlLabel>Proszę wpisać opis sprawy</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.value}
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <HelpBlock>Opis sprawy to informacja, czego sprawa dotyczy, np. "Sprawa o wycięcie drzew przy działce 147A"</HelpBlock>
                        </FormGroup>
                    </form>
                    <h3>
                        {'Po utworzeniu sprawy, będzie możliwe dodanie do niej nowych załączników. '}
                        <br />
                        {'Sprawa będzie miała status oczekującej oraz będzie czekać na przypisanie urzędnika.'}
                    </h3>
                    <Button bsStyle="primary" onClick={this.handleCreateCaseClick}>Załóż nową sprawę</Button>
                </Jumbotron>
            </div>
        );
    }
}
