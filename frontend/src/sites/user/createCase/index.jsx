import {connect} from 'react-redux';
import Container from './createCase';
import {createCase} from '../../../actions/caseActions';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        createCase: (description) => dispatch(createCase(description))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
