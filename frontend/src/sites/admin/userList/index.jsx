import {connect} from 'react-redux';
import Container from './userList';
import {getUsers} from '../../../actions/usersActions';

const mapStateToProps = (state) => {
    return {
        usersList: state.usersReducer.usersList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUsers: () => dispatch(getUsers())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
