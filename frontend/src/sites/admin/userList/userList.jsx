/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron, Tabs, Tab} from "react-bootstrap";
import Loader from '../../../components/shared/loader';
import UserContainer from '../../../components/admin/usersList/container';

export default class EditAddress extends Component {
    state = {
        headerMessage: "Lista użytkowników systemu"
    };

    componentDidMount() {
        const {getUsers} = this.props;
        getUsers();
    };


    render() {
        const {headerMessage} = this.state;
        const {usersList} = this.props;

        if (!usersList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }

        return (
            <div>
                <Jumbotron>
                    {headerMessage}
                </Jumbotron>
                <Tabs defaultActiveKey={1}>
                    <Tab eventKey={1} title="Użytkownicy">
                        <UserContainer data={usersList.users} roleFlag={true}/>
                    </Tab>
                    <Tab eventKey={2} title="Urzędnicy">
                        <UserContainer data={usersList.officials} />
                    </Tab>
                    <Tab eventKey={3} title="Kierownicy">
                        <UserContainer data={usersList.managers} />
                    </Tab>
                </Tabs>
            </div>
        );
    }
}
