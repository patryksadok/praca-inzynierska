import {connect} from 'react-redux';
import Home from './home.jsx';

const mapStateToProps = (state) => {
    return {
        isLogged: state.sessionReducer.accessToken
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
