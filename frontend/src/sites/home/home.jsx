/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import LoggedOut from '../../components/homepage/loggedOut';
import LoggedIn from '../../components/homepage/loggedIn';

export default class Home extends Component {

    render() {
        const {isLogged} = this.props;
        if (!isLogged) {
            return (
                <div>
                    <LoggedOut/>
                </div>
            );
        }
        return (
            <div>
                <LoggedIn/>
            </div>
        );
    }
}
