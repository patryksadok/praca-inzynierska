/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron }  from "react-bootstrap";
import ProfileSection from "../../../components/official/profileSection";
import Loader from "../../../components/shared/loader";

export default class Profile extends Component {
    state = {
        welcomeMessage: "Profil kierownika"
    };

    async componentDidMount() {
        const {getManagerProfile} = this.props;
        await getManagerProfile();
    };

    render() {
        const {welcomeMessage} = this.state;
        const {profileData} = this.props;

        if (!profileData) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }
        
        return (
            <div>
                <Jumbotron>
                    <h2>
                        {welcomeMessage}
                    </h2>
                </Jumbotron>
                <Jumbotron>
                    <ProfileSection
                        data={profileData}
                        header="Dane osobowe"
                    />
                </Jumbotron>
            </div>
        );
    }
}
