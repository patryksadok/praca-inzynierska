import {connect} from 'react-redux';
import Container from './profile';
import {getManagerProfile} from '../../../actions/profileActions';

const mapStateToProps = (state) => {
    return {
        profileData: state.profileReducer.managerProfile
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getManagerProfile: () => dispatch(getManagerProfile())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
