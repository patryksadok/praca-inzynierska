import {connect} from 'react-redux';
import Container from './case';
import {getManagerCaseDetails, assignOfficialToCase, getOfficialsList} from '../../../actions/caseActions';

const mapStateToProps = (state) => {
    return {
        managerCase: state.caseReducer.managerCase,
        officialsList: state.caseReducer.officialsList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getManagerCaseDetails: (id) => dispatch(getManagerCaseDetails(id)),
        assignOfficialToCase: (officialId, id) => dispatch(assignOfficialToCase(officialId, id)),
        getOfficialsList: () => dispatch(getOfficialsList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
