/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import {Jumbotron} from "react-bootstrap";
import Loader from "../../../components/shared/loader";
import SelectModal from '../../../components/shared/selectModal';
import moment from 'moment';

export default class ManagerCase extends Component {
    state = {
        acceptingModal: false,
        acceptingModalHeaderText: "Przypis urzędnika do sprawy"
    };

    componentDidMount() {
        const {match: {params: {id}}, getManagerCaseDetails, getOfficialsList} = this.props;
        getManagerCaseDetails(id);
        getOfficialsList();
    }

    handleCloseModals = () => {
        this.setState({acceptingModal: false});
    };

    openAcceptingModal = async () => {
        await this.setState({acceptingModal: true});
    };

    assignOfficialToCase = (officialId) => {
        this.setState({acceptingModal: false});
        const {assignOfficialToCase, history} = this.props;
        const {id} = this.props.match.params;
        assignOfficialToCase(id, officialId);
        history.push('/manager-cases');
    };

    render() {
        const {match: {params: {id}}, managerCase, officialsList} = this.props;
        const {acceptingModal, acceptingModalHeaderText} = this.state;

        if (!managerCase || !officialsList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500}
                    type="Watch"
                    width={500}
                />
            );
        }
        const updatedAt = moment(managerCase.caseData.caseDetails.updatedAt).format("DD/MM/YYYY");
        const createdAt = moment(managerCase.caseData.caseDetails.createdAt).format("DD/MM/YYYY");

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {managerCase.caseData.caseDetails.description}
                    </h2>
                    <h4>
                        {`Numer sprawy: ${id}`}
                    </h4>
                    <h4>
                        {`Data utworzenia sprawy: ${createdAt}`}
                    </h4>
                    {managerCase.caseData.caseDetails.updatedAt && 
                    <h4>
                        {`Ostatnia aktualizacja: ${updatedAt}`}
                    </h4>
                    }
                    <h4>
                        {`Status sprawy: ${managerCase.caseStatus}`}
                    </h4>
                    {managerCase.caseData.caseDetails.closedAt &&
                        <h4>{`Data zamknięcia: ${moment(managerCase.caseData.caseDetails.closedAt).format("DD/MM/YYYY")}`}</h4>
                    }
                    {managerCase.caseData.clientNames.names &&
                        <h4>
                            {`Dane klienta: ${managerCase.caseData.clientNames.names.firstName} ${managerCase.caseData.clientNames.names.lastName}`}
                        </h4>
                    }
                    {managerCase.caseData.officialNames.names &&
                        <h4>
                            {`Prowadzący sprawę: ${managerCase.caseData.officialNames.names.firstName} ${managerCase.caseData.officialNames.names.lastName}`}
                        </h4>
                    }
                    {managerCase.caseData.caseDetails.verdictId &&
                        <h4>
                            {`Werdykt: ${id}`}
                        </h4>
                    }
                </Jumbotron>
                <Jumbotron>
                    {managerCase.caseStatus === 'Oczekująca' && !managerCase.caseData.caseDetails.officialId && 
                        <h4 onClick={this.openAcceptingModal}>Przypisz urzędnika do sprawy</h4>
                    }
                </Jumbotron>
                <SelectModal opened={acceptingModal} handleClose={this.handleCloseModals} handleConfirm={this.assignOfficialToCase} 
                    headerText={acceptingModalHeaderText} data={officialsList} />
            </div>
        );
    }
}
