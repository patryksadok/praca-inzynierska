/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import {Jumbotron} from "react-bootstrap";
import ProfileForm from '../../components/user/profileForm';
import Loader from "../../components/shared/loader";
import PasswordForm from '../../components/user/passwordForm';
export default class EditProfile extends Component {
    state = {
        bothAddresses: "Adres zameldowania i korespondencji",
        checkboxMessage: "Zaznacz, jeśli adresy korespodencyjny i zameldowania są te same.",
        firstAddress: "Adres korespondencyjny",
        renderSecondForm: true,
        secondAddress: "Adres zameldowania",
        welcomeMessage: "Edycja adresu"
    };

    componentDidMount() {
        const {getProfile} = this.props;
        getProfile();
    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    handleCheckboxChange = () => {
        const {renderSecondForm} = this.state;
        this.setState({renderSecondForm: !renderSecondForm});
    };

    handleRedirectToProfile = () => {
        const {history} = this.props;
        history.push('/profile');
    };

    render() {
        const {welcomeMessage} = this.state;
        const {profileData} = this.props;
        if (!profileData) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }

        return (
            <div>
                <Jumbotron>
                    <h2>
                        {welcomeMessage}
                    </h2>
                    <h4 onClick={this.handleRedirectToProfile}>
                        {"Powrót do profilu"}
                    </h4>
                    <ProfileForm
                        data = {profileData}
                        header="Edycja profilu"
                    />
                </Jumbotron>
                <Jumbotron>
                    <h2>Zmiana hasła</h2>
                    <PasswordForm />
                </Jumbotron>
            </div>
        );
    }
}
