import {connect} from 'react-redux';
import Container from './editProfile';
import {getProfile} from '../../actions/profileActions';

const mapStateToProps = (state) => {
    return {
        profileData: state.profileReducer.profile
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProfile: () => dispatch(getProfile())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
