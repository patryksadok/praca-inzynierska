import React, {Component} from 'react';
import {FormGroup, ControlLabel, Button, FormControl, HelpBlock} from "react-bootstrap";
import { withRouter } from "react-router";

class AddressForm extends Component {
    state = {
        blockCity: "Nazwa miasta (nie może być pusta)",
        blockCountry: "Nazwa państwa (nie może być pusta)",
        blockFlatNumber: "Numer mieszkania (może być pusty)",
        blockHomeNumber: "Numer domu (nie może być pusty)",
        blockPostCode: "Kod pocztowy (nie może być pusty)",
        blockState: "Nazwa województwa (nie może być pusta)",
        blockStreet: "Nazwa ulicy (nie może być pusta)",
        city: '', 
        country: '', 
        disable: false,
        flatNumber: '',
        homeNumber: '',
        labelCity: "Miejscowość: ",
        labelCountry: "Kraj: ",
        labelFlatNumber: "Numer mieszkania: ",
        labelHomeNumber: "Numer domu: ",
        labelPostCode: "Kod pocztowy: ",
        labelState: "Województwo: ",
        labelStreet: "Ulica: ",
        postCode: '',
        states: '',
        street: '',
    };

    static getDerivedStateFromProps(props, state) {
        if (props.data) {
            const {country, city, street, homeNumber, flatNumber} = props.data;
            const states = props.data.state;
            if (country) {
                state = {
                    ...state,
                    country,
                    states,
                    city,
                    street,
                    homeNumber,
                    flatNumber
                };
                return state;
            }
            return null;
        }
    }


    validateInputs = () => {
        const {country, city, street, postCode, homeNumber, states} = this.state;
        this.setState({
            disable: false
        });
        const numberLength = 1, minLength = 3, postCodeLength = 4;
        const stringsTable = [country, city, street, states]; //

        stringsTable.forEach((item) => {
            if (item.length < minLength) {
                this.setState({
                    disable: true
                });
            }
        });

        if (homeNumber.length < numberLength) {
            this.setState({
                disable: true
            });
        }
    };

    handleSubmitForm = async () => {
        const {addressType, updateCheckedAddress, updateCorrespondenceAddress, history} = this.props;
        const {country, city, street, postCode, homeNumber, flatNumber, states} = this.state;
        const data = {
            country,
            city,
            street,
            postCode,
            homeNumber, 
            flatNumber,
            state:states,
            postCode
        };
        if (addressType === "correspondence") {
            await updateCorrespondenceAddress(data);
        }
        if (addressType === "check") {
            await updateCheckedAddress(data);
        }
        if (addressType === "both") {
            await updateCheckedAddress(data);
            await updateCorrespondenceAddress(data);
        }
        history.push('/profile');
    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
        this.validateInputs();
    };

    render() {
        const {header} = this.props;
        const postCode = 22555;
        const {labelCountry, labelCity, labelFlatNumber, labelHomeNumber, labelPostCode,
            labelState, labelStreet, disable, country, states, city, street, homeNumber, flatNumber,
            blockCity, blockCountry, blockFlatNumber, blockHomeNumber, blockPostCode, blockState, blockStreet} = this.state;

        return (
            <div>
                <form>
                    <FormGroup>
                        <h4>
                            {header}
                        </h4>
                        
                        <ControlLabel>
                            {labelCountry}
                        </ControlLabel>
                        <FormControl
                            name="country"
                            onChange={this.handleChange}
                            type="text"
                            value={country}
                        />
                        <HelpBlock>
                            {blockCountry}
                        </HelpBlock>
                        <ControlLabel>
                            {labelState}
                        </ControlLabel>
                        <FormControl
                            name="states"
                            onChange={this.handleChange}
                            type="text"
                            value={states}
                        />
                        <HelpBlock>
                            {blockState}
                        </HelpBlock>
                        <ControlLabel>
                            {labelCity}
                        </ControlLabel>
                        <FormControl
                            name="city"
                            onChange={this.handleChange}
                            type="text"
                            value={city}
                        />
                        <HelpBlock>
                            {blockCity}
                        </HelpBlock>
                        <ControlLabel>
                            {labelStreet}
                        </ControlLabel>
                        <FormControl
                            name="street"
                            onChange={this.handleChange}
                            type="text"
                            value={street}
                        />
                        <HelpBlock>
                            {blockStreet}
                        </HelpBlock>
                        <ControlLabel>
                            {labelHomeNumber}
                        </ControlLabel>
                        <FormControl
                            name="homeNumber"
                            onChange={this.handleChange}
                            type="text"
                            value={homeNumber}
                        />
                        <HelpBlock>
                            {blockHomeNumber}
                        </HelpBlock>
                        <ControlLabel>
                            {labelFlatNumber}
                        </ControlLabel>
                        <FormControl
                            name="flatNumber"
                            onChange={this.handleChange}
                            type="text"
                            value={flatNumber}
                        />
                        <HelpBlock>
                            {blockFlatNumber}
                        </HelpBlock>
                        <ControlLabel>
                            {labelPostCode}
                        </ControlLabel>
                        <FormControl
                            name="postCode"
                            onChange={this.handleChange}
                            type="text"
                            value={postCode}
                        />
                        <HelpBlock>
                            {blockPostCode}
                        </HelpBlock>
                    </FormGroup>
                    <Button
                        bsStyle="primary"
                        disabled={disable}
                        onClick={this.handleSubmitForm}
                    >
                        {'Zapisz adres'}
                    </Button>
                </form>
            </div>
        );
    }
}

export default withRouter(AddressForm);
