import {connect} from 'react-redux';
import Container from './addressForm';
import {updateCheckedAddress, updateCorrespondenceAddress} from '../../../actions/addressActions';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateCheckedAddress: (data) => dispatch(updateCheckedAddress(data)),
        updateCorrespondenceAddress: (data) => dispatch(updateCorrespondenceAddress(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
