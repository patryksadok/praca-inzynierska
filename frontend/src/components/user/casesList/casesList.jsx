import React, {Component} from 'react';
import { Table }  from "react-bootstrap";
import CaseItem from "../caseItem";

export default class CasesList extends Component {
    state = { };

    render() {
        const {data} = this.props;
        
        return (
            <div>
                <Table responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Opis</th>
                            <th>Utworzono</th>
                            <th>Ostatnia aktualizacja</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.map((item, iterator) => {
                                return (
                                    <CaseItem itemData={item} />
                                );
                            })
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}