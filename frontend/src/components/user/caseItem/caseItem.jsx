import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class CaseItem extends Component {
    state = {
    };

    render() {
        const {itemData} = this.props;
        const caseUrl = "/case/"+itemData.id;
        const updatedAt = moment(itemData.updatedAt).format("DD/MM/YYYY");
        const createdAt = moment(itemData.createdAt).format("DD/MM/YYYY");

        return (
            <tr>
                <td>{itemData.id}</td>
                <td>
                    <Link to={caseUrl}>
                        {itemData.description}
                    </Link>
                </td>
                <td>{createdAt}</td>
                <td>{updatedAt}</td>
                {itemData.closedAt &&
                    <td>{'Zamknięta'}</td>
                }
                {!itemData.awaiting && !itemData.closedAt &&
                    <td>{'W toku'}</td>
                }
                {itemData.awaiting && !itemData.closedAt &&
                    <td>{'Oczekująca'}</td>
                }
            </tr>
        );
    };
};
