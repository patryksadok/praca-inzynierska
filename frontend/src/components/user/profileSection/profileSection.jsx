import React, {Component} from 'react';
import {Panel, Jumbotron} from "react-bootstrap";
import { Link } from 'react-router-dom';

export default class ProfileSection extends Component {
    state = {
        emailLabel: "Adres e-mail",
        nameLabel: "Imię",
        surnameLabel: "Nazwisko"
    };

    render() {
        const {emailLabel, nameLabel, surnameLabel} = this.state;
        const {header} = this.props;
        const {data: {email, firstName, lastName}}=this.props;

        return (
            <div>
                <Jumbotron>
                    <h3>
                        {"Dane profilu"}
                    </h3>
                    <Link to="/edit-profile">
                        <h4>
                            {"Edytuj"}
                        </h4>
                    </Link>
                </Jumbotron>
                <Jumbotron>
                    <h3>
                        {header}
                    </h3>
                    <Panel>
                        <Panel.Heading>
                            {nameLabel}
                        </Panel.Heading>
                        <Panel.Body>
                            {firstName}
                        </Panel.Body>
                        <Panel.Heading>
                            {surnameLabel}
                        </Panel.Heading>
                        <Panel.Body>
                            {lastName}
                        </Panel.Body>
                        <Panel.Heading>
                            {emailLabel}
                        </Panel.Heading>
                        <Panel.Body>
                            {email}
                        </Panel.Body>
                    </Panel>
                </Jumbotron>
            </div>
        );
    }
}
