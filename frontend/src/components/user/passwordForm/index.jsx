import {connect} from 'react-redux';
import Container from './passwordForm';
import {changePassword} from '../../../actions/profileActions';

const mapStateToProps = () => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changePassword: (password, newPassword) => dispatch(changePassword(password, newPassword))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
