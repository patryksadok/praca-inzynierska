import React, {Component} from 'react';
import {FormGroup, ControlLabel, Button, FormControl} from "react-bootstrap";
import {withRouter} from "react-router";

class PasswordForm extends Component {
    state = {
        disable: true,
        labelPassword: 'Podaj aktualne hasło',
        labelNewPassword: 'Podaj nowe hasło',
        password: '',
        newPassword: ''
    };

    validateInputs = () => {
        const {password, newPassword} = this.state;
        this.setState({
            disable: false
        });
        const stringsTable = [newPassword,
            password];

        stringsTable.forEach((item) => {
            if (!item || item.trim().length < 7) {
                this.setState({
                    disable: true
                });
            }
        });
    }

    handleSubmitForm = async () => {
        const {changePassword} = this.props;
        const {password, newPassword} = this.state;
        await changePassword(password, newPassword);
        await this.setState({
            password: '',
            newPassword: '',
            disabled: true
        });
    };

    handleChange = async (event) => {
        await this.setState({[event.target.name]: event.target.value});
        await this.validateInputs();
    };

    render() {
        const {header} = this.props;
        const {labelNewPassword, labelPassword, disable, password, newPassword} = this.state;

        return (
            <div>
                <form>
                    <FormGroup>
                        <h4>
                            {header}
                        </h4>
                        <ControlLabel>
                            {labelPassword}
                        </ControlLabel>
                        <FormControl
                            name="password"
                            onChange={this.handleChange}
                            type="password"
                            value={password}
                        />
                        <ControlLabel>
                            {labelNewPassword}
                        </ControlLabel>
                        <FormControl
                            name="newPassword"
                            onChange={this.handleChange}
                            type="password"
                            value={newPassword}
                        />
                    </FormGroup>
                    <Button
                        bsStyle="primary"
                        disabled={disable}
                        onClick={this.handleSubmitForm}
                    >
                        {'Zmień hasło'}
                    </Button>
                </form>
            </div>
        );
    }
}

export default withRouter(PasswordForm);
