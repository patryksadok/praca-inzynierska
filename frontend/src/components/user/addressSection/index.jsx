import {connect} from 'react-redux';
import Container from './addressSection';
import {getAddresses} from '../../../actions/addressActions';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAddresses: () => dispatch(getAddresses())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
