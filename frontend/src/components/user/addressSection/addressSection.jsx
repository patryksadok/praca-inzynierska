import React, {Component} from 'react';
import {Panel, Jumbotron} from "react-bootstrap";


export default class AddressSection extends Component {
    state = {
        cityLabel: "Miejscowość",
        countryLabel: "Kraj",
        flatNumberLabel: "Numer mieszkania",
        homeNumberLabel: "Numer domu",
        postalCodeLabel: "Kod pocztowy",
        stateLabel: "Województwo",
        streetLabel: "Ulica"
    };

    render() {
        const {cityLabel, countryLabel, stateLabel, streetLabel, homeNumberLabel, flatNumberLabel} = this.state;
        const {header} = this.props;
        const {data: {country, city, state, street, homeNumber, flatNumber}} = this.props;

        return (
            <div>
                <Jumbotron>
                    <h3>{header}</h3>
                    <Panel>
                        <Panel.Heading>{countryLabel}</Panel.Heading>
                        <Panel.Body>{country}</Panel.Body>
                        <Panel.Heading>{stateLabel}</Panel.Heading>
                        <Panel.Body>{state}</Panel.Body>
                        <Panel.Heading>{cityLabel}</Panel.Heading>
                        <Panel.Body>{city}</Panel.Body>
                        <Panel.Heading>{streetLabel}</Panel.Heading>
                        <Panel.Body>{street}</Panel.Body>
                        <Panel.Heading>{homeNumberLabel}</Panel.Heading>
                        <Panel.Body>{homeNumber}</Panel.Body>
                        <Panel.Heading>{flatNumberLabel}</Panel.Heading>
                        <Panel.Body>{flatNumber}</Panel.Body>
                    </Panel>
                </Jumbotron>
            </div>
        );
    }
}
