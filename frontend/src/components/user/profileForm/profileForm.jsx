import React, {Component} from 'react';
import {FormGroup, ControlLabel, Button, FormControl, HelpBlock} from "react-bootstrap";
import {withRouter} from "react-router";

class ProfileForm extends Component {
    state = {
        blockFirstName: "Imię (nie może być puste)",
        blockLastName: "Nazwisko (nie może być puste)",
        blockEmail: "Adres e-mail (musi posiadać poprawny format)",
        lastName: '',
        firstName: '',
        email: '',
        disable: false,
        labelFirstName: "Imię: ",
        labelLastName: "Nazwisko: ",
        labelEmail: "Email: "
    };

    static getDerivedStateFromProps(props, state) {
        if (props.data) {
            const {firstName, lastName, email} = props.data;
            if (email) {
                state = {
                    ...state,
                    email,
                    lastName,
                    firstName
                };
                return state;
            }
            return null;
        }
    }


    validateInputs = () => {
        const {email, lastName, firstName} = this.state;
        this.setState({
            disable: false
        });
        const stringsTable = [email,
lastName,
firstName];

        stringsTable.forEach((item) => {
            if (!item) {
                this.setState({
                    disable: true
                });
            }
        });
    }

    handleSubmitForm = () => {
        const {history, updateProfile} = this.props;
        const {email, firstName, lastName} = this.state;
        const data = {
            email,
            firstName,
            lastName
        };
        updateProfile(data);
        history.push('/profile');
    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
        this.validateInputs();
    };

    render() {
        const {header} = this.props;
        const {firstName, lastName, email, blockEmail, blockFirstName, blockLastName, labelEmail, labelFirstName, labelLastName, disable} = this.state;

        return (
            <div>
                <form>
                    <FormGroup>
                        <h4>
                            {header}
                        </h4>
                        <ControlLabel>
                            {labelFirstName}
                        </ControlLabel>
                        <FormControl
                            name="firstName"
                            onChange={this.handleChange}
                            type="text"
                            value={firstName}
                        />
                        <HelpBlock>
                            {blockFirstName}
                        </HelpBlock>
                        <ControlLabel>
                            {labelLastName}
                        </ControlLabel>
                        <FormControl
                            name="lastName"
                            onChange={this.handleChange}
                            type="text"
                            value={lastName}
                        />
                        <HelpBlock>
                            {blockLastName}
                        </HelpBlock>
                        <ControlLabel>
                            {labelEmail}
                        </ControlLabel>
                        <FormControl
                            name="email"
                            onChange={this.handleChange}
                            type="email"
                            value={email}
                        />
                        <HelpBlock>
                            {blockEmail}
                        </HelpBlock>
                    </FormGroup>
                    <Button
                        bsStyle="primary"
                        disabled={disable}
                        onClick={this.handleSubmitForm}
                    >
                        {'Zapisz dane profilu'}
                    </Button>
                </form>
            </div>
        );
    }
}

export default withRouter(ProfileForm);
