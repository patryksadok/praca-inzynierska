import {connect} from 'react-redux';
import Container from './profileForm';
import {updateProfile} from '../../../actions/profileActions';

const mapStateToProps = () => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProfile: (data) => dispatch(updateProfile(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
