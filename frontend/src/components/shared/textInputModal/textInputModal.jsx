import React, {Component} from 'react';
import {Modal, Button, FormGroup, ControlLabel, FormControl} from "react-bootstrap";

export default class ConfirmationModal extends Component {
    state = {
        commentValue: ''
    };

    handleChange = (e) => {
        this.setState({commentValue: e.target.value});
    }

    render() {
        const {opened, headerText, handleClose, handleConfirm} = this.props;
        const {commentValue} = this.state;

        return (
            <div className="static-modal">
                <Modal show={opened} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{headerText}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>Wpisz poniżej treść komentarza:</ControlLabel>
                            <FormControl componentClass="textarea" onChange={this.handleChange} />
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="danger" onClick={handleClose}>Anuluj</Button>
                        <Button bsStyle="success" onClick={() => {handleConfirm(commentValue)}}>Potwierdź</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    };
};
