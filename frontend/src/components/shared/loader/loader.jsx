import React, {Component} from 'react';
import Loader from 'react-loader-spinner';
import { Jumbotron }  from "react-bootstrap";

export default class Loaders extends Component {
    state = { };

    render() {
        const {width, height, type, color} = this.props;
        return (
            <div>
                <Jumbotron>
                    <h2>
                        {'Ładowanie danych, proszę czekać.'}
                    </h2>
                    <Loader
                        color={color}
                        height={height}	
                        type={type}
                        width={width}
                    />  
                </Jumbotron>
            </div>
        )
    }
}