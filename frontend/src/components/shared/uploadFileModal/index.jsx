import {connect} from 'react-redux';
import Container from './uploadFileModal';

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = () => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
