import React, {Component} from 'react';
import {Modal, Button} from "react-bootstrap";
import moment from 'moment';
export default class FileUploadModal extends Component {
    state = {
        selectedFile: null,
        loaded: 0
    };

    handleUploadFile = (ev) => {
        ev.preventDefault();
        const {handleConfirm} = this.props;
        const data = new FormData();
        if (this.uploadInput.files[0].name.includes('.pdf')) {
            const nameTables = this.uploadInput.files[0].name.split('.pdf');
            data.append('file', this.uploadInput.files[0]);
            data.append('filename', nameTables[0]+`-date-${moment.now()}.pdf`);
            handleConfirm(data);
        } else {
            console.log('error');
        }
    }

    handleSelectedFile = async (event) => {
        await this.setState({
            selectedFile: event.target.files[0],
            loaded: 0
        });
    };

    handleUpload = () => {
        const {handleConfirm} = this.props;

        let fileToSend = this.state.selectedFile
        const nameTable = uploadedFile.name.split('.pdf');
        
        handleConfirm({fileToSend, name});
    };

    render() {
        const {opened, headerText, handleClose} = this.props;

        return (
            <div className="static-modal">
                <Modal show={opened} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{headerText}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={this.handleUploadFile}>
                            <input ref={(ref) => { this.uploadInput = ref; }} type="file" />
                            <br />
                            <button>Wyślij plik</button>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="danger" onClick={handleClose}>Anuluj</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    };
};
