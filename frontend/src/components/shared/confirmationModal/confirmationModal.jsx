import React, {Component} from 'react';
import {Modal, Button} from "react-bootstrap";

export default class ConfirmationModal extends Component {
    render() {
        const {opened, bodyText, headerText, handleClose, handleConfirm} = this.props;

        return (
            <div className="static-modal">
                <Modal show={opened} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{headerText}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>
                            {bodyText}
                        </p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="danger" onClick={handleClose}>Anuluj</Button>
                        <Button bsStyle="success" onClick={handleConfirm}>Potwierdź</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    };
};
