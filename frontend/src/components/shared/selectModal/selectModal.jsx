import React, {Component} from 'react';
import {Modal, Button, FormGroup, ControlLabel, FormControl} from "react-bootstrap";

export default class ConfirmationModal extends Component {
    state = {
        selectedValue: null
    };

    static getDerivedStateFromProps(props, state) {
        const {data} = props;
        state = {
            ...state,
            data 
        };

        return state;
    };

    handleSelectChange = (event) => {
        this.setState({selectedValue: event.target.value});
    };

    render() {
        const {opened, headerText, handleClose, handleConfirm} = this.props;
        const {selectedValue, data} = this.state;

        return (
            <div className="static-modal">
                <Modal show={opened} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{headerText}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup controlId="formControlsSelect">
                            <ControlLabel>Wybierz opcję z listy:</ControlLabel>
                            {data[0].firstName && 
                                <FormControl componentClass="select" onChange={this.handleSelectChange} >
                                    <option value="0">Wybierz opcję</option>
                                    {
                                        data.map((item, iterator) => {
                                            return (
                                                <option value={item.id}>{item.firstName} {item.lastName}</option>
                                            );
                                        })
                                    }
                                </FormControl>
                            }
                            {data[0].type &&
                                <FormControl componentClass="select" onChange={this.handleSelectChange} >
                                    <option value="0">Wybierz opcję</option>
                                    {
                                        data.map((item, iterator) => {
                                            return (
                                                <option value={item.id}>{item.type}</option>
                                            );
                                        })
                                    }
                                </FormControl>
                            }
                            
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="danger" onClick={handleClose}>Anuluj</Button>
                        <Button bsStyle="success" onClick={() => {handleConfirm(selectedValue)}} disabled={!this.state.selectedValue > 0 ? true : false}>Potwierdź</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    };
};
