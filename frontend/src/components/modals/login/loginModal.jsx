import React, {Component} from "react";
import { Modal, Button, FormGroup, ControlLabel, FormControl, HelpBlock} from "react-bootstrap";

export default class LoginModal extends Component {
    state = {
        closeButton: "Zamknij",
        emailFlag: false,
        emailLabel: "Adres e-mail",
        emailPlaceholder: "Prawidłowy adres email",
        emailValue: "",
        enableValidator: false,
        inputMessage: "Proszę podać adres e-mail oraz hasło",
        passwordFlag: false,
        passwordLabel: "Hasło",
        passwordPlaceholder: "Hasło",
        passwordValue: "",
        loginButton: "Zaloguj",
        loginMessage: "Okno logowania"
    }

    handleChange = (name) => (event) => {
        this.setState({
            enableValidator: true,
            [name]: event.target.value
        });
        if (name === "emailValue") {
            this.validateEmail(event.target.value);
        }
        if (name === "passwordValue") {
            this.validatePassword(event.target.value);
        }
    }

    handleLoginUser = async () => {
        const {loginUser, onHide, getContext} = this.props;
        const {emailValue, passwordValue} = this.state;
        try {
            await loginUser(emailValue, passwordValue);
            await getContext();
            onHide(true);
        } catch (error) {
            this.setState({
                emailValue: '',
                passwordValue: '',
                enableValidator: false,
                emailFlag: 'false',
                passwordFlag: 'false'
            })
        }
    }

    validateFlags = () => {
        const {emailFlag, passwordFlag} = this.state;
        if (emailFlag && passwordFlag) {
            return true;
        }
        return false;
    }

    validatePassword = (value) => {
        const emptyPassword = 0;
        this.setState({
            passwordFlag: true,
            passwordLabel: "Hasło"
        });
        if (value.length === emptyPassword) {
            this.setState({
                passwordFlag: false,
                passwordLabel: "Hasło nie może być puste"
            });
        }
    }

    validateEmail = (value) => {
        const emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.setState({
            emailFlag: true,
            emailLabel: "Adres e-mail"
        });
        if (!value.match(emailFormat)) {
            this.setState({
                emailFlag: false,
                emailLabel: "Podany e-mail posiada nieprawidłowy format"
            });
        }
        if (!value) {
            this.setState({
                emailFlag: false,
                emailLabel: "Adres e-mail nie może być pusty"
            });
        }
        return true;
    }

    getValidationStatePasswords() {
        const {passwordFlag, enableValidator} = this.state;
        if (enableValidator) {
            if (!passwordFlag) {
                return 'error';
            }
            if (passwordFlag) {
                return 'success';
            }
        }
        return null;
    }

    getValidationStateEmail() {
        const {emailFlag, enableValidator} = this.state;
        if (enableValidator) {
            if (!emailFlag) return 'error';
            if (emailFlag) return 'success';
        }
        return null;
    }

    render() {
        const {loginMessage, inputMessage, emailLabel, passwordLabel, emailPlaceholder,
            passwordPlaceholder, emailValue, passwordValue, validationMessage, closeButton, loginButton} = this.state;

        return (
            <Modal
                {...this.props}
                aria-labelledby="contained-modal-title-sm"
                bsSize="large"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">
                        {loginMessage}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>
                        {inputMessage}
                    </h4>
                    <form>
                        <FormGroup validationState={this.getValidationStateEmail()}>
                            <ControlLabel>
                                {emailLabel}
                            </ControlLabel>
                            <FormControl
                                onChange={this.handleChange("emailValue")}
                                placeholder={emailPlaceholder}
                                type="email"
                                value={emailValue}
                            />
                            <FormControl.Feedback />
                        </FormGroup>
                        <FormGroup validationState={this.getValidationStatePasswords()}>
                            <ControlLabel>
                                {passwordLabel}
                            </ControlLabel>
                            <FormControl
                                onChange={this.handleChange("passwordValue")}
                                placeholder={passwordPlaceholder}
                                type="password"
                                value={passwordValue}
                            />
                            <FormControl.Feedback />
                            <HelpBlock>
                                {validationMessage}
                            </HelpBlock>
                        </FormGroup>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHide} >
                        {closeButton}
                    </Button>
                    <Button
                        disabled={!this.validateFlags()}
                        onClick={this.handleLoginUser}
                    >
                        {loginButton}
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}