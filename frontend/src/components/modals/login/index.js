import {connect} from 'react-redux';
import LoginModal from './loginModal';
import {loginUser} from '../../../actions/sessionActions';
import {getContext} from '../../../actions/contextActions';

const mapStateToProps = (state) => {
    return {
        
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getContext: () => dispatch(getContext()),
        loginUser: (email, password) => dispatch(loginUser(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
