import {connect} from 'react-redux';
import RegisterModal from './registerModal';
import {register} from '../../../actions/sessionActions';

const mapStateToProps = (state) => {
    const {registerSuccess} = state.sessionReducer;
    return {
        registerSuccess
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: (email, password) => dispatch(register(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterModal);
