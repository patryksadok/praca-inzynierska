import React, {Component} from 'react';
import moment from 'moment';

export default class AttachmentItem extends Component {
    state = { };

    render() {
        const {itemData} = this.props;
        const formattedCreatedAt = moment(itemData.createdAt).format("DD/MM/YYYY");
        
        return (
            <tr>
                <td>{itemData.value}</td>
                <td>{formattedCreatedAt}</td>
                <td>{itemData.authorId}</td>
            </tr>
        );
    };
};
