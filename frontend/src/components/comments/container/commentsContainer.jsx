import React, {Component} from 'react';
import { Table, Jumbotron }  from "react-bootstrap";
import CommentItem from '../item';

export default class CommentsContainer extends Component {
    state = { };

    render() {
        const {data} = this.props;
        
        return (
            <div>
                <Jumbotron>
                    <h2>Komentarze:</h2>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Opis</th>
                                <th>Utworzono</th>
                                <th>Autor</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((item, iterator) => {
                                    return (
                                        <CommentItem itemData={item} />
                                    );
                                })
                            }
                        </tbody>
                    </Table>
                </Jumbotron>
                
            </div>
        )
    }
}