import React, {Component} from 'react';
import { Table, Jumbotron }  from "react-bootstrap";
import UserItem from '../item';

export default class UserContainer extends Component {
    state = { };

    render() {
        const {data, roleFlag} = this.props;
        
        return (
            <div>
                <Jumbotron>
                    <h2>Załączniki:</h2>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Imię</th>
                                <th>Nazwisko</th>
                                {roleFlag &&
                                    <th>Przypisywanie roli</th>
                                }
                                {!roleFlag &&
                                    <th>Usuwanie roli</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((item, iterator) => {
                                    return (
                                        <UserItem itemData={item} roleFlag={roleFlag}/>
                                    );
                                })
                            }
                        </tbody>
                    </Table>
                </Jumbotron>
                
            </div>
        )
    }
}