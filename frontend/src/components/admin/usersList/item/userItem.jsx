import React, {Component} from 'react';
import { withRouter } from "react-router";
import ConfirmationModal from '../../../shared/confirmationModal';
import SelectionModal from '../../../shared/selectModal';

class AttachmentItem extends Component {
    state = {
        confirmationModal: false,
        confirmationModalHeader: "Potwierdzanie usuwania roli",
        confirmationModalBody: "Czy chcesz usunąć temu użytkownikowi rolę?",
        selectionModalHeader: "Przypisywanie nowej roli użytkownikowi systemu",
        selectionModalBody: "Wybierz rolę dla użytkownika",
        selectionModal: false 
    };

    componentDidMount() {
        const {roleFlag} = this.props;
        if (roleFlag) {
            const {getRoles} = this.props;
            getRoles();
        }
    };

    handleOpenConfirmationModal = () => {
        this.setState({confirmationModal: true});
    };

    handleOpenSelectionModal = () => {
        this.setState({selectionModal: true});
    };

    handleCloseModals = () => {
        this.setState({confirmationModal: false});
    };

    handleAcceptAttachment = async () => {
        const {acceptAttachment, itemData: {id}, history} = this.props;
        await acceptAttachment(id);
        await history.push(history.location);
    };

    handleRemoveRole = () => {
        const {itemData, removeRole} = this.props;
        this.setState({confirmationModal: false});
        removeRole(itemData.id);
    };

    handleAssignRole = (id) => {
        const {itemData, assignRole} = this.props;
        this.setState({selectionModal: false});
        assignRole(itemData.id, id);
    };

    render() {
        const {itemData, roleFlag, roles} = this.props;
        const {confirmationModal, confirmationModalBody, confirmationModalHeader, selectionModal, selectionModalHeader} = this.state;
        if (itemData.firstName !== "Konto") {
            return (
                <tr>
                    <td>{itemData.firstName}</td>
                    <td>{itemData.lastName}</td>
                    {roleFlag && 
                        <td onClick={this.handleOpenSelectionModal}>Przypisz rolę</td>
                    }
                    {!roleFlag && 
                        <td onClick={this.handleOpenConfirmationModal}>Usuń rolę</td>
                    }
                    <ConfirmationModal opened={confirmationModal} handleClose={this.handleCloseModals} handleConfirm={this.handleRemoveRole}
                        headerText={confirmationModalHeader} bodyText={confirmationModalBody} />
                    {roles &&
                        <SelectionModal opened={selectionModal} handleClose={this.handleCloseModals} handleConfirm={this.handleAssignRole} 
                            headerText={selectionModalHeader} data={roles}/>
                    }
                </tr>
            );
        }
        return null;
    };
};

export default withRouter(AttachmentItem);
