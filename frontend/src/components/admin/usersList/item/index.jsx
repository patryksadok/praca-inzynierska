import {connect} from 'react-redux';
import Container from './userItem';
import {removeRole, getRoles, assignRole} from '../../../../actions/profileActions';

const mapStateToProps = (state) => {
    return {
        roles: state.profileReducer.roles
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        assignRole: (profileId, roleId) => dispatch(assignRole(profileId, roleId)),
        getRoles: () => dispatch(getRoles()),
        removeRole: (id) => dispatch(removeRole(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
