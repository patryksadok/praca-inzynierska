import React, {Component} from 'react';
import moment from 'moment';
import ConfirmationModal from '../../shared/confirmationModal';
import { withRouter } from "react-router";

class AttachmentItem extends Component {
    state = { 
        confirmationModal: false,
        confirmationModalHeader: "Potwierdzanie akceptacji załącznika",
        confirmationModalBody: "Czy chcesz potwierdzić ten załącznik?"
    };
    
    handleDownloadAttachment = () => {
        const {itemData, dowloadAttachment} = this.props;
        dowloadAttachment(itemData.id);
    }

    handleOpenModal = () => {
        const {activeRole, itemData} = this.props;
        if (activeRole === 'Urzędnik' && !itemData.confirmed) {
            this.setState({confirmationModal: true});
        }
    };

    handleAcceptAttachment = async () => {
        const {acceptAttachment, itemData: {id}, history} = this.props;
        await acceptAttachment(id);
        await this.setState({confirmationModal: false});
        await history.push(history.location);
    };

    handleCloseModals = () => {
        this.setState({confirmationModal: false});
    };

    render() {
        const {confirmationModal, confirmationModalHeader, confirmationModalBody} = this.state;
        const {itemData} = this.props;
        const createdAt = moment(itemData.createdAt).format("DD/MM/YYYY");
        const confirmationStatus = itemData.confirmed ? 'Tak' : 'Nie';
        return (
            <tr>
                <td onClick={this.handleDownloadAttachment}>{itemData.description}</td>
                <td>{createdAt}</td>
                <td onClick={this.handleOpenModal}>{confirmationStatus}</td>
                <td>autor</td>
                <ConfirmationModal opened={confirmationModal} handleClose={this.handleCloseModals} handleConfirm={this.handleAcceptAttachment}
                    headerText={confirmationModalHeader} bodyText={confirmationModalBody} />
            </tr>
        );
    };
};

export default withRouter(AttachmentItem);
