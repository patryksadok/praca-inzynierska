import {connect} from 'react-redux';
import Container from './attachmentItem';
import {dowloadAttachment, acceptAttachment} from '../../../actions/fileActions';

const mapStateToProps = (state) => {
    return {
        activeRole: state.contextReducer.activeContext.activeRole
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        dowloadAttachment: (id) => dispatch(dowloadAttachment(id)),
        acceptAttachment: (id) => dispatch(acceptAttachment(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
