import React, {Component} from 'react';
import { Table, Jumbotron }  from "react-bootstrap";
import AttachmentItem from '../item';

export default class AttachmentsContainer extends Component {
    state = { };

    render() {
        const {data} = this.props;
        
        return (
            <div>
                <Jumbotron>
                    <h2>Załączniki:</h2>
                    <Table responsive>
                        <thead>
                            <tr>
                                <th>Opis</th>
                                <th>Utworzono</th>
                                <th>Zatwierdzono</th>
                                <th>Autor</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((item, iterator) => {
                                    return (
                                        <AttachmentItem itemData={item} />
                                    );
                                })
                            }
                        </tbody>
                    </Table>
                </Jumbotron>
                
            </div>
        )
    }
}