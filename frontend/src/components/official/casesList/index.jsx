import {connect} from 'react-redux';
import {getOfficialCasesList} from '../../../actions/caseActions';
import Container from './casesList';

const mapStateToProps = (state) => {
    return {
        casesList: state.caseReducer.officialCasesList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getOfficialCasesList: () => dispatch(getOfficialCasesList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
