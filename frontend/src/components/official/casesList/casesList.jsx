import React, {Component} from 'react';
import {Tabs, Tab}  from "react-bootstrap";
import CasesInProgress from '../caseContainers/casesInProgress';
import CasesClosed from '../caseContainers/casesClosed';
import CasesAwaiting from '../caseContainers/casesAwaiting';
//import CasesAwaitingToAssign from '../caseContainers/casesAwaitingToAssign';
import Loader from "../../shared/loader";

export default class CasesList extends Component {
    componentDidMount() {
        const {getOfficialCasesList} = this.props;
        getOfficialCasesList();
    }

    render() {
        const {casesList} = this.props;
        
        if (!casesList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }
        
        return (
            <div>
                <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                    <Tab eventKey={1} title="Oczekujące na zatwierdzenie przez urzędnika">
                        <CasesAwaiting data={casesList.awaitingCases} />
                    </Tab>
                    <Tab eventKey={2} title="W toku">
                        <CasesInProgress data={casesList.inprogressCases} />
                    </Tab>
                    <Tab eventKey={3} title="Zakończone">
                        <CasesClosed data={casesList.closedCases} />
                    </Tab>
                </Tabs>
            </div>
        )
    }
}