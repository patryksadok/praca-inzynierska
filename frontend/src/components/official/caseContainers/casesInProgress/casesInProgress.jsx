import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import {Table} from "react-bootstrap";
import CaseItem from '../../caseItem/inProgress';

export default class CasesInProgress extends Component {
    render() {
        const {data} = this.props;

        return (
            <Table responsive>
                <thead>
                    <tr>
                        <th>Tytuł</th>
                        <th>Data zgłoszenia sprawy</th>
                        <th>Data ostatniej aktualizacji</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((item, iterator) => {
                            return (
                                <CaseItem itemData={item} />
                            );
                        })
                    }
                </tbody>
            </Table>
        );
    };
};
