import {connect} from 'react-redux';
import {getManagerCasesList} from '../../../actions/caseActions';
import Container from './casesList';

const mapStateToProps = (state) => {
    return {
        casesList: state.caseReducer.managerCasesList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getManagerCasesList: () => dispatch(getManagerCasesList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
