import React, {Component} from 'react';
import {Tabs, Tab}  from "react-bootstrap";
import CasesInProgress from '../casesContainer/inProgress';
import CasesClosed from '../casesContainer/closed';
import CasesAwaiting from '..//casesContainer/awaiting';
import Loader from "../../shared/loader";

export default class CasesList extends Component {
    componentDidMount() {
        const {getManagerCasesList} = this.props;
        getManagerCasesList();
    }

    render() {
        const {casesList} = this.props;

        if (!casesList) {
            return (
                <Loader
                    color="#00BFFF"
                    height={500} 
                    type="Watch" 
                    width={500} 
                />
            );
        }
        
        return (
            <div>
                <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                    <Tab eventKey={1} title="Oczekujące na przypisanie">
                        <CasesAwaiting data={casesList.awaitingToAssignCases} />
                    </Tab>
                    <Tab eventKey={2} title="Oczekujące na zatwierdzenie">
                        <CasesAwaiting data={casesList.awaitingCases} />
                    </Tab>
                    <Tab eventKey={3} title="W toku">
                        <CasesInProgress data={casesList.inprogressCases} />
                    </Tab>
                    <Tab eventKey={4} title="Zakończone">
                        <CasesClosed data={casesList.closedCases} />
                    </Tab>
                </Tabs>
            </div>
        )
    }
}
