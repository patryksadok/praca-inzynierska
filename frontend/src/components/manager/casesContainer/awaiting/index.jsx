import {connect} from 'react-redux';
import Container from './casesAwaiting';

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = () => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
