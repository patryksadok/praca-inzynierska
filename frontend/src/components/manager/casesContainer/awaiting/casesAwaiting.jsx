import React, {Component} from 'react';
import {Table} from "react-bootstrap";
import CaseItem from '../../caseItem/awaiting';

export default class CasesAwaiting extends Component<{}> {
    render() {
        const {data} = this.props;

        return (
            <Table responsive>
                <thead>
                    <tr>
                        <th>Tytuł</th>
                        <th>Data zgłoszenia sprawy</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((item, iterator, key) => {
                            return (
                                <CaseItem itemData={item} key={item.id}/>
                            );
                        })
                    }
                </tbody>
            </Table>
        );
    };
};
