import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { withRouter } from "react-router";

class CaseItem extends Component {
    render() {
        const {itemData} = this.props;
        const caseUrl = "/case-manager/"+itemData.id;
        const updatedAt = moment(itemData.updatedAt).format("DD/MM/YYYY");
        const createdAt = moment(itemData.createdAt).format("DD/MM/YYYY");
        const officialId = 1;
        const officialUrl = "/official/"+officialId;
        return (
            <tr>
                <td onClick={this.handleRedirectToCase}>
                    <Link to={caseUrl}>
                        {itemData.description}
                    </Link>
                </td>
                <td>{createdAt}</td>
                <td>{updatedAt}</td>
            </tr>
        );
    };
};

export default withRouter(CaseItem);
