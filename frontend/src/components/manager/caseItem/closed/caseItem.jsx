import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class CaseItem extends Component {
    render() {
        const {itemData} = this.props;
        const caseUrl = "/case-manager/"+itemData.id;
        const closedAt = moment(itemData.closedAt).format("DD/MM/YYYY");
        const createdAt = moment(itemData.createdAt).format("DD/MM/YYYY");
        
        return (
            <tr>
                <td onClick={this.handleRedirectToCase}>
                    <Link to={caseUrl}>
                        {itemData.description}
                    </Link>
                </td>
                <td>{createdAt}</td>
                <td>{closedAt}</td>
            </tr>
        );
    };
};
