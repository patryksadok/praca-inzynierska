import {connect} from 'react-redux';
import LoggedIn from './loggedIn.jsx';
import {logoutUser} from "../../../actions/sessionActions";


const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logoutUser: (userId) => dispatch(logoutUser(userId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoggedIn);
