import React, {Component} from 'react';
import { Jumbotron} from "react-bootstrap";

export default class LoggedOut extends Component {
    state = {
        headerMessage: "Pomyślnie zalogowano w systemie"
    };


    render() {
        const {contentMessage, headerMessage} = this.state;
        return (
            <div>
                <Jumbotron>
                    <h2>
                        {headerMessage}
                    </h2>
                    <h3>
                        {contentMessage}
                    </h3>
                </Jumbotron>
            </div>
        );
    }
}
