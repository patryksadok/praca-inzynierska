/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import { Jumbotron} from "react-bootstrap";
import RegisterModal from '../../modals/register';

export default class LoggedOut extends Component {
    state = {
        headerMessage: "Patryk Sadok - Informatyka Uniwersytet Rzeszowski",
        registerModalShow: false,
        statusMessage: "Zarejestruj się, aby zacząć korzystać z funkcjonalności aplikacji",
        titleMessage: "Aplikacja do zarządzania obiegiem dokumentów w jednostce administracyjnej na przykładzie starostwa powiatowego"
    };

    handleInvokeRegisterModal = () => {
        this.setState({
            registerModalShow: true
        });
    }

    render() {
        const registerModalClose = () => this.setState({ registerModalShow: false});
        const {titleMessage, headerMessage, statusMessage, registerModalShow} = this.state;
        return (
            <div>
                <Jumbotron>
                    <h1>
                        {titleMessage}
                    </h1>
                    <h2>
                        {headerMessage}
                    </h2>
                    <p
                        className="login-message-div"
                        onClick={this.handleInvokeRegisterModal}
                    >
                        {statusMessage}
                    </p>
                </Jumbotron>

                <RegisterModal
                    onHide={registerModalClose}
                    show={registerModalShow}
                />
            </div>
        );
    }
};
