import {connect} from 'react-redux';
import LoggedOut from './loggedOut.jsx';
import {loginUser, register} from '../../../actions/sessionActions';


const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (email, password) => dispatch(loginUser(email, password)),
        register: (email, password) => dispatch(register(email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoggedOut);
