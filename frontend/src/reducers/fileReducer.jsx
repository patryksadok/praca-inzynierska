import {
    UPLOAD_ATTACHMENT_BEGIN,
    UPLOAD_ATTACHMENT_FAILURE,
    UPLOAD_ATTACHMENT_SUCCESS,
    ACCEPT_ATTACHMENT_BEGIN,
    ACCEPT_ATTACHMENT_FAILURE,
    ACCEPT_ATTACHMENT_SUCCESS
} from '../actions/configs/fileConfig';

const initialState = {
    error: null,
    uploading: null,
    accepting: null
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPLOAD_ATTACHMENT_BEGIN:
            return {
                ...state,
                error: null,
                uploading: true
            };
        case UPLOAD_ATTACHMENT_SUCCESS:
            return {
                ...state,
                uploading: false
            };
        case UPLOAD_ATTACHMENT_FAILURE:
            return {
                ...state,
                error: action.payload,
                uploading: false
            };
        case ACCEPT_ATTACHMENT_BEGIN:
            return {
                ...state,
                error: null,
                accepting: true
            };
        case ACCEPT_ATTACHMENT_SUCCESS:
            return {
                ...state,
                accepting: false
            };
        case ACCEPT_ATTACHMENT_FAILURE:
            return {
                ...state,
                error: action.payload,
                accepting: false
            };
        default:
            return state;
    }
};

export default profileReducer;
