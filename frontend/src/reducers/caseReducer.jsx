import {
    GET_CASES_BEGIN,
    GET_CASES_FAILURE,
    GET_CASES_SUCCESS,
    GET_CASE_BEGIN,
    GET_CASE_FAILURE,
    GET_CASE_SUCCESS,
    CASE_CLOSE_BEGIN,
    CASE_CLOSE_FAILURE,
    CASE_CLOSE_SUCCESS,
    CASE_CREATE_BEGIN,
    CASE_CREATE_FAILURE,
    CASE_CREATE_SUCCESS,
    GET_OFFICIAL_CASES_BEGIN,
    GET_OFFICIAL_CASES_FAILURE,
    GET_OFFICIAL_CASES_SUCCESS,
    GET_OFFICIAL_CASE_BEGIN,
    GET_OFFICIAL_CASE_FAILURE,
    GET_OFFICIAL_CASE_SUCCESS,
    CONFIRM_CASE_BEGIN,
    CONFIRM_CASE_FAILURE,
    CONFIRM_CASE_SUCCESS,
    GET_MANAGER_CASES_BEGIN,
    GET_MANAGER_CASES_FAILURE,
    GET_MANAGER_CASES_SUCCESS,
    GET_MANAGER_CASE_BEGIN,
    GET_MANAGER_CASE_FAILURE,
    GET_MANAGER_CASE_SUCCESS,
    GET_OFFICIALS_BEGIN,
    GET_OFFICIALS_FAILURE,
    GET_OFFICIALS_SUCCESS
} from '../actions/configs/caseConfig';

const initialState = {
    userCasesList: null,
    officialCasesList: null,
    managerCasesList: null,
    officialsList: null,
    userCase: null,
    officialCase: null,
    managerCase: null,
    error: null,
    closing: null
};

const caseReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CASES_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_CASES_SUCCESS:
            return {
                ...state,
                loading: false,
                userCasesList: action.payload.cases
            };
        case GET_CASES_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false
            };
        case GET_OFFICIAL_CASES_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_OFFICIAL_CASES_SUCCESS:
            return {
                ...state,
                loading: false,
                officialCasesList: action.payload.cases
            };
        case GET_OFFICIAL_CASES_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false
            };
        case GET_MANAGER_CASES_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_MANAGER_CASES_SUCCESS:
            return {
                ...state,
                loading: false,
                managerCasesList: action.payload.cases
            };
        case GET_MANAGER_CASES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
                managerCasesList: null
            };
        case GET_CASE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_CASE_SUCCESS:
            return {
                ...state,
                loading: false,
                userCase: action.payload.caseData
            };
        case GET_CASE_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false
            };
        case GET_OFFICIAL_CASE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_OFFICIAL_CASE_SUCCESS:
            return {
                ...state,
                loading: false,
                officialCase: action.payload.caseData
            };
        case GET_OFFICIAL_CASE_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false,
                officialCase: null
            };
        case CASE_CLOSE_BEGIN:
            return {
                ...state,
                error: null,
                closing: true
            };
        case CASE_CLOSE_SUCCESS:
            return {
                ...state,
                closing: false,
            };
        case CASE_CLOSE_FAILURE:
            return {
                ...state,
                error: action.payload,
                closing: false
            };
        case CASE_CREATE_BEGIN:
            return {
                ...state,
                error: null,
                creating: true
            };
        case CASE_CREATE_SUCCESS:
            return {
                ...state,
                creating: false,
            };
        case CASE_CREATE_FAILURE:
            return {
                ...state,
                error: action.payload,
                creating: false
            };
        case CONFIRM_CASE_BEGIN:
            return {
                ...state,
                error: null,
                closing: true
            };
        case CONFIRM_CASE_SUCCESS:
            return {
                ...state,
                closing: false,
            };
        case CONFIRM_CASE_FAILURE:
            return {
                ...state,
                error: action.payload,
                closing: false
            };
        case GET_MANAGER_CASE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_MANAGER_CASE_SUCCESS:
            return {
                ...state,
                loading: false,
                managerCase: action.payload.caseData
            };
        case GET_MANAGER_CASE_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false
            };
        case GET_OFFICIALS_BEGIN:
            return {
                ...state,
                officialsList: null,
                error: null,
                loading: true
            };
        case GET_OFFICIALS_SUCCESS:
            return {
                ...state,
                officialsList: action.payload.officialsList,
                error: null,
                loading: false
            };
        case GET_OFFICIALS_FAILURE:
            return {
                ...state,
                errror: action.payload,
                loading: false
            };
        default:
            return state;
    }
};

export default caseReducer;