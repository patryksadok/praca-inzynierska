import {
    GET_CONTEXT_BEGIN,
    GET_CONTEXT_FAILURE,
    GET_CONTEXT_SUCCESS,
    GET_CONTEXT_LIST_BEGIN,
    GET_CONTEXT_LIST_FAILURE,
    GET_CONTEXT_LIST_SUCCESS,
    SET_CONTEXT_BEGIN,
    SET_CONTEXT_FAILURE,
    SET_CONTEXT_SUCCESS
} from '../actions/configs/contextConfig';

const initialState = {
    contextList: [],
    activeContext: '',
    error: null,
    fetching: false
};

const contextReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_CONTEXT_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_CONTEXT_SUCCESS:
            return {
                ...state,
                loading: false,
                activeContext: action.payload
            };
        case GET_CONTEXT_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false,
                activeContext: ''
            };
        case GET_CONTEXT_LIST_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_CONTEXT_LIST_SUCCESS:
            return {
                ...state,
                contextList: action.payload,
                loading: false
            };
        case GET_CONTEXT_LIST_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false,
                profile: null
            };
        case SET_CONTEXT_BEGIN:
            return {
                ...state,
                error: null
            };
        case SET_CONTEXT_SUCCESS:
            return {
                ...state,
                activeContext: action.payload,
                closing: false
            };
        case SET_CONTEXT_FAILURE:
            return {
                ...state,
                error: action.payload,
                closing: false
            };
        default:
            return state;
    }
};

export default contextReducer;