import {
    ADD_COMMENT_BEGIN,
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_SUCCESS
} from '../actions/configs/commentConfig';
    
const initialState = {
    adding: false,
    error: null
};
    
const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_COMMENT_BEGIN:
            return {
                ...state,
                error: null,
                adding: true
            };
        case ADD_COMMENT_SUCCESS:
            return {
                ...state,
                adding: false
            };
        case ADD_COMMENT_FAILURE:
            return {
                ...state,
                error: action.payload,
                adding: false
            };
        default:
            return state;
    }
};

export default commentReducer;
