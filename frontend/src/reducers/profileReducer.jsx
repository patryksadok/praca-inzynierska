import {
    GET_PROFILE_BEGIN,
    GET_PROFILE_FAILURE,
    GET_PROFILE_SUCCESS,
    GET_OFFICIAL_PROFILE_BEGIN,
    GET_OFFICIAL_PROFILE_FAILURE,
    GET_OFFICIAL_PROFILE_SUCCESS,
    GET_MANAGER_PROFILE_BEGIN,
    GET_MANAGER_PROFILE_FAILURE,
    GET_MANAGER_PROFILE_SUCCESS,
    REMOVE_ROLE_BEGIN,
    REMOVE_ROLE_FAILURE,
    REMOVE_ROLE_SUCCESS,
    GET_ROLES_BEGIN,
    GET_ROLES_FAILURE,
    GET_ROLES_SUCCESS
} from '../actions/configs/profileConfig';

const initialState = {
    profile: null,
    officialProfile: null,
    managerProfile: null,
    error: null,
    roles: null
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PROFILE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_PROFILE_SUCCESS:
            return {
                ...state,
                loading: false,
                profile: action.payload
            };
        case GET_PROFILE_FAILURE:
            return {
                ...state,
                profile: null,
                error: action.payload,
                loading: false
            };
        case GET_OFFICIAL_PROFILE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_OFFICIAL_PROFILE_SUCCESS:
            return {
                ...state,
                loading: false,
                officialProfile: action.payload
            };
        case GET_OFFICIAL_PROFILE_FAILURE:
            return {
                ...state,
                officialProfile: null,
                error: action.payload,
                loading: false
            };
        case GET_MANAGER_PROFILE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_MANAGER_PROFILE_SUCCESS:
            return {
                ...state,
                loading: false,
                managerProfile: action.payload
            };
        case GET_MANAGER_PROFILE_FAILURE:
            return {
                ...state,
                managerProfile: null,
                error: action.payload,
                loading: false
            };
        case REMOVE_ROLE_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case REMOVE_ROLE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case REMOVE_ROLE_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false
            };
        case GET_ROLES_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_ROLES_SUCCESS:
            return {
                ...state,
                loading: false,
                roles: action.payload.roles
            };
        case GET_ROLES_FAILURE:
            return {
                ...state,
                roles: null,
                error: action.payload,
                loading: false
            };
        default:
            return state;
    }
};

export default profileReducer;
