import {GET_ADDRESS_BEGIN, GET_ADDRESS_FAILURE, GET_ADDRESS_SUCCESS,
UPDATE_ADDRESS_BEGIN, UPDATE_ADDRESS_FAILURE, UPDATE_ADDRESS_SUCCESS} from '../actions/configs/addressConfig';

const initialState = {
    checkedAddress: null,
    correspondenceAddress: null
};

const addressReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ADDRESS_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_ADDRESS_SUCCESS:
            return {
                ...state,
                checkedAddress: action.payload.checkedAddress,
                correspondenceAddress: action.payload.correspondenceAddress,
                loading: false
            };
        case GET_ADDRESS_FAILURE:
            return {
                ...state,
                checkedAddress: {},
                correspondenceAddress: {},
                error: action.payload,
                loading: false
            };
        case UPDATE_ADDRESS_BEGIN:
            return {
                ...state,
                error: null,
                loading: true,
                updatedAddress: false
            };
        case UPDATE_ADDRESS_SUCCESS:
            return {
                ...state,
                updatedAddress: true,
                loading: false
            };
        case UPDATE_ADDRESS_FAILURE:
            return {
                ...state,
                accessToken: null,
                error: action.payload,
                loading: false
            };
        default:
            return state;
    }
};

export default addressReducer;