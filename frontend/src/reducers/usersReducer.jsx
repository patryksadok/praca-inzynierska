import {
    GET_USERS_BEGIN,
    GET_USERS_FAILURE,
    GET_USERS_SUCCESS
} from "../actions/configs/usersConfig";

const initialState = {
    loading: false,
    error: null,
    usersList: null
};

const sessionReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USERS_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case GET_USERS_SUCCESS:
            return {
                ...state,
                usersList: action.payload,
                loading: false
            };
        case GET_USERS_FAILURE:
            return {
                ...state,
                usersList: null,
                error: action.payload.error,
                loading: false
            };
        default:
            return state;
    }
};

export default sessionReducer;
