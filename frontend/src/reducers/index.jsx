import {combineReducers} from 'redux';
import dataReducer from './dataReducer';
import sessionReducer from './sessionReducer';
import addressReducer from './addressReducer';
import profileReducer from './profileReducer';
import caseReducer from './caseReducer';
import contextReducer from './contextReducer';
import commentReducer from './commentReducer';
import fileReducer from './fileReducer';
import usersReducer from './usersReducer';

const reducers = combineReducers({
    addressReducer,
    usersReducer,
    caseReducer,
    commentReducer,
    contextReducer,
    fileReducer,
    data: dataReducer,
    profileReducer,
    sessionReducer
});

export default reducers;
