import {
    DESTROY_SESSION,
    LOGIN_USER_BEGIN,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER_BEGIN,
    LOGOUT_USER_FAILURE,
    LOGOUT_USER_SUCCESS,
    REGISTER_BEGIN,
    REGISTER_FAILURE,
    REGISTER_SUCCESS,
    SET_CURRENT_USER
} from "../actions/configs/sessionConfig";

const initialState = {
    registerSuccess: false,
    user: {},
    accessToken: null,
    tokenError: null
};

const sessionReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                accessToken: action.user
            };
        case LOGIN_USER_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                accessToken: action.payload.user.accessToken,
                userId: action.payload.user.userId,
                loading: false
            };
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                accessToken: null,
                error: action.payload.error,
                loading: false
            };
        case LOGOUT_USER_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };
        case LOGOUT_USER_SUCCESS:
            return {
                ...state,
                accessToken: null,
                loading: false
            };
        case LOGOUT_USER_FAILURE:
            return {
                ...state,
                accessToken: null,
                error: action.payload,
                loading: false
            };
        case DESTROY_SESSION:
            return {
                ...state,
                accessToken: null,
                loading: false
            };
        case REGISTER_BEGIN:
            return {
                ...state,
                error: null,
                loading: true,
                registerSuccess: false
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                loading: false,
                registerSuccess: true
            };
        case REGISTER_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false,
                registerSuccess: false

            };
        default:
            return state;
    }
};

export default sessionReducer;
