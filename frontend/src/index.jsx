import React from 'react';
import {applyMiddleware, compose, createStore} from 'redux';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import {install} from 'offline-plugin/runtime';
import {Provider} from 'react-redux';
import reducers from './reducers';
import App from './sites/app';
import {setCurrentUser} from './actions/dispatches/sessionDispatches';

const store = createStore(reducers,
    compose(applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : (f) => f
    ));

if (localStorage.getItem('accessToken')) {
    store.dispatch(setCurrentUser(localStorage.getItem('accessToken')));
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('app')
);

if (process.env.NODE_ENV === 'production') {
    install();
}
