import {
    addCommentBegin,
    addCommentFailure,
    addCommentSuccess
} from './dispatches/commentDispatches';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors} from "./shared";

export const addComment = (id, comment) => {
    return (dispatch) => {
        dispatch(addCommentBegin());
        return axios.
            post(`/comment-case/${id}`, {comment}).
            then((res) => {
                handleErrors(res);
                dispatch(addCommentSuccess());
                toast.success("Pomyślnie dodano komentarz");
            }).
            catch((error) => {
                dispatch(addCommentFailure(error.response.data));
                toast.error("Wystąpił błąd podczas dodawania komentarza");
            });
    };
};
