import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors, handleResponseError} from "./shared";
import {
    getCasesBegin,
    getCasesFailure,
    getCasesSuccess,
    getCaseBegin,
    getCaseFailure,
    getCaseSuccess,
    closeCaseBegin,
    closeCaseFailure,
    closeCaseSuccess,
    createCaseSuccess,
    createCaseBegin,
    createCaseFailure,
    getOfficialCasesBegin,
    getOfficialCasesFailure,
    getOfficialCasesSuccess,
    getOfficialCaseBegin,
    getOfficialCaseFailure,
    getOfficialCaseSuccess,
    confirmCaseBegin,
    confirmCaseFailure,
    confirmCaseSuccess,
    getManagerCasesBegin,
    getManagerCasesFailure,
    getManagerCasesSuccess,
    getManagerCaseBegin,
    getManagerCaseFailure,
    getManagerCaseSuccess,
    getOfficialsBegin,
    getOfficialsFailure,
    getOfficialsSuccess,
    assignOfficialBegin,
    assignOfficialFailure,
    assignOfficialSuccess
} from "./dispatches/caseDispatches";

export const getUserCasesList = () => {
    return (dispatch) => {
        dispatch(getCasesBegin());
        return axios.
            get(`/cases-list`).
            then((res) => {
                handleErrors(res);
                dispatch(getCasesSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getCasesFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania listy spraw");
            });
    };
};

export const getUserCaseDetails = (id) => {
    return (dispatch) => {
        dispatch(getCaseBegin());
        return axios.
            get(`/case/${id}`).
            then((res) => {
                handleErrors(res);
                let {caseData} = res.data;
                const caseStatus = setCaseStatus(res.data.caseData.caseDetails);
                caseData = {caseStatus, caseData};
                let data = {caseData};
                dispatch(getCaseSuccess(data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania sprawy");
            });
    };
};

const setCaseStatus = (caseParams) => {
    if (caseParams.closedAt) {
        return 'Zamknięta';
    }
    if (caseParams.awaiting) {
        return 'Oczekująca';
    }
    if (!caseParams.awaiting) {
        return 'W toku';
    }
};

export const closeCase = (id) => {
    return (dispatch) => {
        dispatch(closeCaseBegin());
        return axios.
            post(`/case/${id}`).
            then((res) => {
                handleErrors(res);
                dispatch(closeCaseSuccess());
                toast.success("Pomyślnie zamknięto sprawę");
            }).
            catch((error) => {
                dispatch(closeCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas zamykania sprawy");
            });
    };
};

export const createCase = (description) => {
    return (dispatch) => {
        dispatch(createCaseBegin());
        return axios.
            post(`/case-create`, {description}).
            then((res) => {
                handleErrors(res);
                dispatch(createCaseSuccess());
                toast.success("Pomyślnie utworzono sprawę. Przekierowaliśmy Cię do listy Twoich spraw.");
            }).
            catch((error) => {
                dispatch(createCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas tworzenia sprawy");
            });
    };
};

export const getOfficialCasesList = () => {
    return (dispatch) => {
        dispatch(getOfficialCasesBegin());
        return axios.
            get(`/official-cases-list`).
            then((res) => {
                handleErrors(res);
                dispatch(getOfficialCasesSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getOfficialCasesFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania listy spraw");
            });
    };
};

export const getOfficialCaseDetails = (id) => {
    return (dispatch) => {
        dispatch(getOfficialCaseBegin());
        return axios.
            get(`/case-official/${id}`).
            then((res) => {
                handleErrors(res);
                let {caseData} = res.data;
                const caseStatus = setCaseStatus(res.data.caseData.caseDetails);
                caseData = {caseStatus, caseData};
                let data = {caseData};
                dispatch(getOfficialCaseSuccess(data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getOfficialCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania sprawy");
            });
    };
};

export const confirmCase = (id) => {
    return (dispatch) => {
        dispatch(confirmCaseBegin());
        return axios.
            post(`/case-confirm/${id}`).
            then((res) => {
                handleErrors(res);
                dispatch(confirmCaseSuccess());
                toast.success("Pomyślnie zatwierdzono sprawę");
            }).
            catch((error) => {
                dispatch(confirmCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas zatwierdzania sprawy");
            });
    };
};

export const getManagerCasesList = () => {
    return (dispatch) => {
        dispatch(getManagerCasesBegin());
        return axios.
            get(`/manager-cases-list`).
            then((res) => {
                handleErrors(res);
                dispatch(getManagerCasesSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getManagerCasesFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania listy spraw");
            });
    };
};

export const getManagerCaseDetails = (id) => {
    return (dispatch) => {
        dispatch(getManagerCaseBegin());
        return axios.
            get(`/case-manager/${id}`).
            then((res) => {
                handleErrors(res);
                let {caseData} = res.data;
                const caseStatus = setCaseStatus(res.data.caseData.caseDetails);
                caseData = {caseStatus, caseData};
                let data = {caseData};
                dispatch(getManagerCaseSuccess(data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getManagerCaseFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania sprawy");
            });
    };
};

export const getOfficialsList = () => {
    return (dispatch) => {
        dispatch(getOfficialsBegin());
        return axios.
            get(`/officials-list`).
            then((res) => {
                handleErrors(res);
                dispatch(getOfficialsSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getOfficialsFailure(error.response.data));
                toast.error("Wystąpił błąd podczas pobierania listy urzędników");
            });
    };
};

export const assignOfficialToCase = (officialId, id) => {
    return (dispatch) => {
        dispatch(assignOfficialBegin());
        return axios.
            post(`/assign-official`, {id, officialId}).
            then((res) => {
                handleErrors(res);
                dispatch(assignOfficialSuccess());
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(assignOfficialFailure(error.response.data));
                toast.error("Wystąpił błąd podczas przypisywania urzędnika do sprawy");
            });
    };
};