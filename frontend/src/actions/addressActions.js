import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors} from "./shared";
import {updateAddressBegin, updateAddressFailure, updateAddressSuccess, getAddressBegin, getAddressSuccess, getAddressFailure} from "./dispatches/addressDispatches";

export const updateCheckedAddress = (data) => {
    return (dispatch) => {
        dispatch(updateAddressBegin());
        return axios.
            patch(`/address-checked/edit`, {data}).
            then((res) => {
                handleErrors(res);
                dispatch(updateAddressSuccess());
                toast.success("Pomyślnie zapisano adres zameldowania");
            }).
            catch((error) => {
                dispatch(updateAddressFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie zapisu adresu");
            });
    };
};

export const updateCorrespondenceAddress = (data) => {
    return (dispatch) => {
        dispatch(updateAddressBegin());
        return axios.
            patch(`/address-correspondence/edit`, {data}).
            then((res) => {
                handleErrors(res);
                dispatch(updateAddressSuccess());
                toast.success("Pomyślnie zapisano adres korespondencji");
            }).
            catch((error) => {
                dispatch(updateAddressFailure(error.response.data));
                toast.success("Wystąpił błąd w trakcie zapisu adresu");
            });
    };
};

export const getAddresses = () => {
    return (dispatch) => {
        dispatch(getAddressBegin());
        return axios.
            get(`/address`).
            then((res) => {
                handleErrors(res);
                dispatch(getAddressSuccess(res.data));
            }).
            catch((error) => {
                dispatch(getAddressFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania adresów");
            });
    };
}