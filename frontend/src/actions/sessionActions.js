import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {registerBegin, registerFailure, registerSuccess, loginUserFailure, loginUserSuccess, loginUserBegin,
    logoutUserBegin, logoutUserFailure, logoutUserSuccess} from "./dispatches/sessionDispatches";
import {handleErrors} from "./shared";

export const loginUser = (email, password) => {
    return (dispatch) => {
        dispatch(loginUserBegin());

        return axios.
            post(`/user/login`, {
                email,
                password
            }).
            then((res) => {
                handleErrors(res);
                dispatch(
                    loginUserSuccess({
                        accessToken: res.data.accessToken,
                        userId: res.data.userId
                    })
                );

                localStorage.setItem("accessToken", res.data.accessToken);
                localStorage.setItem("refreshToken", res.data.refreshToken);
                localStorage.setItem("userId", res.data.userId);
                toast.success("Pomyślnie zalogowano jako " + email);

                return {user: res.data.access_token};
            }).
            catch((error) => {
                console.log(error);
                dispatch(loginUserFailure(error.response.data));
                switch (error.response.data.message) {
                    case "Required data not found":
                        toast.error("Nie podano wszystkich danych służących do rejestracji.");
                        break;
                    case "Error in SQL syntax.":
                        toast.error("Wystąpił błąd z bazą danych.");
                        break;
                    case "Invalid password":
                        toast.error("Podano nieprawidłowe hasło.");
                        break;
                    case "User not found in database":
                        toast.error("W bazie nie ma takiego użytkownika o mailu. " + email);
                        break;
                    default:
                        toast.error("Wystąpił nieznany błąd.");
                }
            });
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        dispatch(logoutUserBegin());
        return axios.
            get(`/user/logout`).
            then((res) => {
                handleErrors(res);
                dispatch(logoutUserSuccess());
            }).
            catch((error) => {
                dispatch(logoutUserFailure(error.response.data));
                localStorage.removeItem("accessToken");
                localStorage.removeItem("refreshToken");
                localStorage.removeItem("userId");
                toast.success("Pomyślnie wylogowano");
            }).
            finally(() => {
                localStorage.removeItem("accessToken");
                localStorage.removeItem("refreshToken");
                localStorage.removeItem("userId");
                toast.success("Pomyślnie wylogowano");
            });
    };
};

export const register = (email, password) => {
    return (dispatch) => {
        dispatch(registerBegin());
        return axios.
            put(`/user/register`, {email, password}).
            then((res) => {
                handleErrors(res);
                dispatch(registerSuccess());
                toast.success("Pomyślnie zarejestrowano nowego użytkownika: " + email);
            }).
            catch((error) => {
                if (!error.response) {
                    toast.error("Wystąpił błąd połączenia z serwerem w trakcie rejestracji.");
                }

                dispatch(registerFailure(error.response.data));

                const errorMessage = error.response.data.message;
                switch (errorMessage) {
                    case "Required data not found":
                        toast.error("Nie podano wszystkich danych służących do rejestracji.");
                        break;
                    case "Error in SQL syntax.":
                        toast.error("Wystąpił błąd z bazą danych.");
                        break;
                    case "User already exists in database":
                        toast.error("Użytkownik o mailu " + email + " jest już zarejestrowany w bazie.");
                        break;          
                    default:
                        toast.error("Wystąpił nieznany błąd w trakcie rejestracji.");
                }
            });
    };
};
