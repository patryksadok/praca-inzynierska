import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors, handleResponseError} from "./shared";
import {
    uploadFileBegin,
    uploadFileFailure,
    uploadFileSuccess,
    downloadAttachmentBegin,
    downloadAttachmentFailure,
    downloadAttachmentSuccess,
    acceptAttachmentBegin,
    acceptAttachmentFailure,
    acceptAttachmentSuccess
} from './dispatches/fileDispatches';

export const uploadAttachment = (data, caseId) => {
    return (dispatch) => {
        dispatch(uploadFileBegin());
        return axios.
            post(`/attachment/${caseId}`, data).
            then((res) => {
                handleErrors(res);
                dispatch(uploadFileSuccess(res.data));
                toast.success("Pomyślnie dodano załącznik");
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(uploadFileFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie dodawania załącznika");
            });
    };
};

export const dowloadAttachment = (id) => {
    return (dispatch) => {
        dispatch(downloadAttachmentBegin());
        return axios.
            get(`/attachment/${id}`).
            then((res) => {
                handleErrors(res);
                dispatch(downloadAttachmentSuccess());
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(downloadAttachmentFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania załącznika");
            });
    };
};

export const acceptAttachment = (id) => {
    return (dispatch) => {
        dispatch(acceptAttachmentBegin());
        return axios.
            post(`/accept-attachment/`, {id}).
            then((res) => {
                handleErrors(res);
                dispatch(acceptAttachmentSuccess());
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(acceptAttachmentFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie zmiany statusu załącznika");
            });
    };
};