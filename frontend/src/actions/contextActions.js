import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors, handleResponseError} from "./shared";
import {
    getContextBegin,
    getContextFailure,
    getContextSuccess,
    getContextListBegin,
    getContextListFailure,
    getContextListSuccess,
    setContextBegin,
    setContextFailure,
    setContextSuccess
} from './dispatches/contextDispatches';

export const getContext = () => {
    return (dispatch) => {
        dispatch(getContextBegin());
        return axios.
            get(`/context`).
            then((res) => {
                handleErrors(res);
                dispatch(getContextSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getContextFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania danych o kontekście");
            });
    };
};

export const getContextList = () => {
    return (dispatch) => {
        dispatch(getContextListBegin());
        return axios.
            get(`/context-list`).
            then((res) => {
                handleErrors(res);
                dispatch(getContextListSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getContextListFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania danych o liście kontekstów");
            });
    };
};

export const setContext = (contextId) => {
    return (dispatch) => {
        dispatch(setContextBegin(contextId));
        return axios.
            put(`/context`, {contextId}).
            then((res) => {
                handleErrors(res);
                dispatch(setContextSuccess(res.data));
                toast.success("Pomyślnie zmieniono kontekst i przekierowano na stronę główną.");
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(setContextFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie ustawiania kontekstu");
            });
    };
};
