import {UPDATE_ADDRESS_BEGIN, UPDATE_ADDRESS_FAILURE, UPDATE_ADDRESS_SUCCESS,
    GET_ADDRESS_BEGIN, GET_ADDRESS_SUCCESS, GET_ADDRESS_FAILURE} from "../configs/addressConfig";

export const updateAddressBegin = () => ({
    type: UPDATE_ADDRESS_BEGIN
});

export const updateAddressSuccess = () => ({
    type: UPDATE_ADDRESS_SUCCESS
});

export const updateAddressFailure = (error) => ({
    payload: error,
    type: UPDATE_ADDRESS_FAILURE
});

export const getAddressBegin = () => ({
    type: GET_ADDRESS_BEGIN
});

export const getAddressFailure = (error) => ({
    payload: error,
    type: GET_ADDRESS_FAILURE
});

export const getAddressSuccess = (addresses) => ({
    payload: addresses,
    type: GET_ADDRESS_SUCCESS
});
