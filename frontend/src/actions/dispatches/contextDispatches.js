import {
    GET_CONTEXT_BEGIN,
    GET_CONTEXT_FAILURE,
    GET_CONTEXT_LIST_BEGIN,
    GET_CONTEXT_LIST_FAILURE,
    GET_CONTEXT_LIST_SUCCESS,
    GET_CONTEXT_SUCCESS,
    SET_CONTEXT_BEGIN,
    SET_CONTEXT_FAILURE,
    SET_CONTEXT_SUCCESS
} from '../configs/contextConfig';

export const getContextBegin = () => ({
    type: GET_CONTEXT_BEGIN
});

export const getContextFailure = (error) => ({
    payload: error,
    type: GET_CONTEXT_FAILURE
});

export const getContextSuccess = (context) => ({
    payload: context,
    type: GET_CONTEXT_SUCCESS
});

export const getContextListBegin = () => ({
    type: GET_CONTEXT_LIST_BEGIN
});

export const getContextListFailure = (error) => ({
    payload: error,
    type: GET_CONTEXT_LIST_FAILURE
});

export const getContextListSuccess = (contextList) => ({
    payload: contextList,
    type: GET_CONTEXT_LIST_SUCCESS
});

export const setContextBegin = () => ({
    type: SET_CONTEXT_BEGIN
});

export const setContextFailure = (error) => ({
    type: SET_CONTEXT_FAILURE,
    payload: error
});

export const setContextSuccess = (newContext) => ({
    payload: newContext,
    type: SET_CONTEXT_SUCCESS
});
