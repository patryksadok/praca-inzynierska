import {
    GET_USERS_BEGIN,
    GET_USERS_FAILURE,
    GET_USERS_SUCCESS
} from '../configs/usersConfig';

export const getUsersBegin = () => ({
    type: GET_USERS_BEGIN
});

export const getUsersFailure = (error) => ({
    payload: error,
    type: GET_USERS_FAILURE
});

export const getUsersSuccess = (users) => ({
    payload: users.usersList,
    type: GET_USERS_SUCCESS
});
