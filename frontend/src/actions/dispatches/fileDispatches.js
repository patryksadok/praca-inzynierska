import {
    UPLOAD_ATTACHMENT_BEGIN,
    UPLOAD_ATTACHMENT_FAILURE,
    UPLOAD_ATTACHMENT_SUCCESS,
    DOWNLOAD_ATTACHMENT_BEGIN,
    DOWNLOAD_ATTACHMENT_FAILURE,
    DOWNLOAD_ATTACHMENT_SUCCESS,
    ACCEPT_ATTACHMENT_BEGIN,
    ACCEPT_ATTACHMENT_FAILURE,
    ACCEPT_ATTACHMENT_SUCCESS
} from '../configs/fileConfig';

export const uploadFileBegin = () => ({
    type: UPLOAD_ATTACHMENT_BEGIN
});

export const uploadFileFailure = (error) => ({
    payload: error,
    type: UPLOAD_ATTACHMENT_FAILURE
});

export const uploadFileSuccess = () => ({
    type: UPLOAD_ATTACHMENT_SUCCESS
});

export const downloadAttachmentBegin = () => ({
    type: DOWNLOAD_ATTACHMENT_BEGIN
});

export const downloadAttachmentSuccess = () => ({
    type: DOWNLOAD_ATTACHMENT_SUCCESS
});

export const downloadAttachmentFailure = (error) => ({
    payload: error,
    type: DOWNLOAD_ATTACHMENT_FAILURE
});

export const acceptAttachmentBegin = () => ({
    type: ACCEPT_ATTACHMENT_BEGIN
});

export const acceptAttachmentFailure = (error) => ({
    payload: error,
    type: ACCEPT_ATTACHMENT_FAILURE
});

export const acceptAttachmentSuccess = () => ({
    type: ACCEPT_ATTACHMENT_SUCCESS
});
