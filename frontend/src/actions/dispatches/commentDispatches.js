import {
    ADD_COMMENT_BEGIN,
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_SUCCESS
} from '../configs/commentConfig';

export const addCommentBegin = () => ({
    type: ADD_COMMENT_BEGIN
});

export const addCommentFailure = (error) => ({
    payload: error,
    type: ADD_COMMENT_FAILURE
});

export const addCommentSuccess = () => ({
    type: ADD_COMMENT_SUCCESS
});
