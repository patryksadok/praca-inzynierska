import {
    GET_PROFILE_BEGIN,
    GET_PROFILE_FAILURE,
    GET_PROFILE_SUCCESS,
    UPDATE_PROFILE_BEGIN,
    UPDATE_PROFILE_FAILURE,
    UPDATE_PROFILE_SUCCESS,
    GET_OFFICIAL_PROFILE_BEGIN,
    GET_OFFICIAL_PROFILE_FAILURE,
    GET_OFFICIAL_PROFILE_SUCCESS,
    GET_MANAGER_PROFILE_BEGIN,
    GET_MANAGER_PROFILE_FAILURE,
    GET_MANAGER_PROFILE_SUCCESS,
    CHANGE_PASSWORD_BEGIN,
    CHANGE_PASSWORD_FAILURE,
    CHANGE_PASSWORD_SUCCESS,
    REMOVE_ROLE_BEGIN,
    REMOVE_ROLE_FAILURE,
    REMOVE_ROLE_SUCCESS,
    GET_ROLES_BEGIN,
    GET_ROLES_FAILURE,
    GET_ROLES_SUCCESS,
    ASSIGN_ROLE_BEGIN,
    ASSIGN_ROLE_FAILURE,
    ASSIGN_ROLE_SUCCESS
} from '../configs/profileConfig';

export const getProfileBegin = () => {
    return {
        type: GET_PROFILE_BEGIN
    };
};

export const getProfileSuccess = (profile) => {
    return {
        payload: profile,
        type: GET_PROFILE_SUCCESS
    };
};

export const getProfileFailure = (error) => {
    return {
        payload: error,
        type: GET_PROFILE_FAILURE
    };
};

export const getOfficialProfileBegin = () => {
    return {
        type: GET_OFFICIAL_PROFILE_BEGIN
    };
};

export const getOfficialProfileSuccess = (profile) => {
    return {
        payload: profile,
        type: GET_OFFICIAL_PROFILE_SUCCESS
    };
};

export const getOfficialProfileFailure = (error) => {
    return {
        payload: error,
        type: GET_OFFICIAL_PROFILE_FAILURE
    };
};

export const updateProfileBegin = () => {
    return {
        type: UPDATE_PROFILE_BEGIN
    };
};

export const updateProfileSuccess = (modified) => {
    return {
        payload: modified,
        type: UPDATE_PROFILE_SUCCESS
    };
};

export const updateProfileFailure =  (error) => {
    return {
        payload: error,
        type: UPDATE_PROFILE_FAILURE
    };
};

export const getManagerProfileBegin = () => {
    return {
        type: GET_MANAGER_PROFILE_BEGIN
    };
};

export const getManagerProfileSuccess = (profile) => {
    return {
        payload: profile,
        type: GET_MANAGER_PROFILE_SUCCESS
    };
};

export const getManagerProfileFailure = (error) => {
    return {
        payload: error,
        type: GET_MANAGER_PROFILE_FAILURE
    };
};

export const changePasswordBegin = () => {
    return {
        type: CHANGE_PASSWORD_BEGIN
    };
};

export const changePasswordFailure = (error) => {
    return {
        payload: error,
        type: CHANGE_PASSWORD_FAILURE
    };
};

export const changePasswordSuccess = () => {
    return {
        type: CHANGE_PASSWORD_SUCCESS
    };
};

export const removeRoleBegin = () => {
    return {
        type: REMOVE_ROLE_BEGIN
    };
};

export const removeRoleFailure = (error) => {
    return {
        payload: error,
        type: REMOVE_ROLE_FAILURE
    };
};

export const removeRoleSuccess = () => {
    return {
        type: REMOVE_ROLE_SUCCESS
    };
};

export const getRolesBegin = () => {
    return {
        type: GET_ROLES_BEGIN
    };
};

export const getRolesFailure = (error) => {
    return {
        payload: error,
        type: GET_ROLES_FAILURE
    };
};

export const getRolesSuccess = (roles) => {
    return {
        payload: roles,
        type: GET_ROLES_SUCCESS
    };
};

export const assignRoleBegin = () => {
    return {
        type: ASSIGN_ROLE_BEGIN
    };
};

export const assignRoleFailure = (error) => {
    return {
        payload: error,
        type: ASSIGN_ROLE_FAILURE
    };
};

export const assignRoleSuccess = () => {
    return {
        type: ASSIGN_ROLE_FAILURE
    };
};
