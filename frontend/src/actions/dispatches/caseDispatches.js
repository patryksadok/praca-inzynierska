import {
    GET_CASES_BEGIN, 
    GET_CASES_FAILURE,
    GET_CASES_SUCCESS,
    GET_CASE_BEGIN,
    GET_CASE_FAILURE,
    GET_CASE_SUCCESS,
    CASE_CLOSE_BEGIN,
    CASE_CLOSE_FAILURE,
    CASE_CLOSE_SUCCESS,
    CASE_CREATE_BEGIN,
    CASE_CREATE_FAILURE,
    CASE_CREATE_SUCCESS,
    GET_OFFICIAL_CASES_BEGIN,
    GET_OFFICIAL_CASES_FAILURE,
    GET_OFFICIAL_CASES_SUCCESS,
    GET_OFFICIAL_CASE_BEGIN,
    GET_OFFICIAL_CASE_FAILURE,
    GET_OFFICIAL_CASE_SUCCESS,
    CONFIRM_CASE_BEGIN,
    CONFIRM_CASE_FAILURE,
    CONFIRM_CASE_SUCCESS,
    GET_MANAGER_CASES_BEGIN,
    GET_MANAGER_CASES_FAILURE,
    GET_MANAGER_CASES_SUCCESS,
    GET_MANAGER_CASE_BEGIN,
    GET_MANAGER_CASE_FAILURE,
    GET_MANAGER_CASE_SUCCESS,
    GET_OFFICIALS_BEGIN,
    GET_OFFICIALS_FAILURE,
    GET_OFFICIALS_SUCCESS,
    ASSIGN_OFFICIAL_BEGIN,
    ASSIGN_OFFICIAL_FAILURE,
    ASSIGN_OFFICIAL_SUCCESS
} from "../configs/caseConfig";

export const getCasesBegin = () => ({
    type: GET_CASES_BEGIN
});

export const getCasesFailure = (error) => ({
    payload: error,
    type: GET_CASES_FAILURE
});

export const getCasesSuccess = (casesList) => ({
    payload: casesList,
    type: GET_CASES_SUCCESS
});

export const getCaseBegin = () => ({
    type: GET_CASE_BEGIN
});

export const getCaseFailure = (error) => ({
    payload: error,
    type: GET_CASE_FAILURE
});

export const getCaseSuccess = (casesList) => ({
    payload: casesList,
    type: GET_CASE_SUCCESS
});

export const closeCaseBegin = () => ({
    type: CASE_CLOSE_BEGIN
});

export const closeCaseFailure = (error) => ({
    payload: error,
    type: CASE_CLOSE_FAILURE
});

export const closeCaseSuccess = () => ({
    type: CASE_CLOSE_SUCCESS
});

export const createCaseBegin = () => ({
    type: CASE_CREATE_BEGIN
});

export const createCaseFailure = (error) => ({
    payload: error,
    type: CASE_CREATE_FAILURE
});

export const createCaseSuccess = (createdCase) => ({
    payload: createdCase,
    type: CASE_CREATE_SUCCESS
});

export const getOfficialCasesBegin = () => ({
    type: GET_OFFICIAL_CASES_BEGIN
});

export const getOfficialCasesFailure = (error) => ({
    payload: error,
    type: GET_OFFICIAL_CASES_FAILURE
});

export const getOfficialCasesSuccess = (casesList) => ({
    payload: casesList,
    type: GET_OFFICIAL_CASES_SUCCESS
});

export const getOfficialCaseBegin = () => ({
    type: GET_OFFICIAL_CASE_BEGIN
});

export const getOfficialCaseFailure = (error) => ({
    payload: error,
    type: GET_OFFICIAL_CASE_FAILURE
});

export const getOfficialCaseSuccess = (officialCase) => ({
    payload: officialCase,
    type: GET_OFFICIAL_CASE_SUCCESS
});

export const confirmCaseBegin = () => ({
    type: CONFIRM_CASE_BEGIN
});

export const confirmCaseFailure = (error) => ({
    payload: error,
    type: CONFIRM_CASE_FAILURE
});

export const confirmCaseSuccess = () => ({
    type: CONFIRM_CASE_SUCCESS
});

export const getManagerCasesBegin = () => ({
    type: GET_MANAGER_CASES_BEGIN
});

export const getManagerCasesFailure = (error) => ({
    payload: error,
    type: GET_MANAGER_CASES_FAILURE
});

export const getManagerCasesSuccess = (casesList) => ({
    payload: casesList,
    type: GET_MANAGER_CASES_SUCCESS
});

export const getManagerCaseBegin = () => ({
    type: GET_MANAGER_CASE_BEGIN
});

export const getManagerCaseFailure = (error) => ({
    payload: error,
    type: GET_MANAGER_CASE_FAILURE
});

export const getManagerCaseSuccess = (managerCase) => ({
    payload: managerCase,
    type: GET_MANAGER_CASE_SUCCESS
});

export const getOfficialsBegin = () => ({
    type: GET_OFFICIALS_BEGIN
});

export const getOfficialsFailure = (error) => ({
    type: GET_OFFICIALS_FAILURE,
    payload: error
});

export const getOfficialsSuccess = (listOfOfficials) => ({
    payload: listOfOfficials,
    type: GET_OFFICIALS_SUCCESS
});

export const assignOfficialBegin = () => ({
    type: ASSIGN_OFFICIAL_BEGIN
});

export const assignOfficialFailure = (error) => ({
    payload: error,
    type: ASSIGN_OFFICIAL_FAILURE
});

export const assignOfficialSuccess = () => ({
    type: ASSIGN_OFFICIAL_SUCCESS
});
