import {
    SET_CURRENT_USER,
    REGISTER_BEGIN,
    REGISTER_FAILURE,
    REGISTER_SUCCESS,
    LOGIN_USER_BEGIN,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER_BEGIN,
    LOGOUT_USER_FAILURE,
    LOGOUT_USER_SUCCESS,
    DESTROY_SESSION,
    REFRESH_TOKEN_BEGIN,
    REFRESH_TOKEN_FAILURE,
    REFRESH_TOKEN_SUCCESS,
    DEPLOY_TOKEN_ERROR,
    ERASE_TOKEN_ERROR
} from "../configs/sessionConfig";

export const setCurrentUser = (user) => {
    return {
        type: SET_CURRENT_USER,
        user
    };
};

export const registerBegin = () => ({
    type: REGISTER_BEGIN
});

export const registerSuccess = () => ({
    type: REGISTER_SUCCESS
});

export const registerFailure = (error) => ({
    payload: error,
    type: REGISTER_FAILURE
});

export const loginUserBegin = () => ({
    type: LOGIN_USER_BEGIN
});

export const loginUserSuccess = (user) => ({
    payload: {user},
    type: LOGIN_USER_SUCCESS
});

export const loginUserFailure = (error) => ({
    payload: {error},
    type: LOGIN_USER_FAILURE
});

export const destroySession = () => ({
    type: DESTROY_SESSION
});

export const logoutUserBegin = () => ({
    type: LOGOUT_USER_BEGIN
});

export const logoutUserSuccess = () => ({
    type: LOGOUT_USER_SUCCESS
});

export const logoutUserFailure = (error) => ({
    payload: { error },
    type: LOGOUT_USER_FAILURE
});

export const refreshTokenBegin = () => ({
    type: REFRESH_TOKEN_BEGIN
});

export const refreshTokenSuccess = (user) => ({
    payload: {user},
    type: REFRESH_TOKEN_SUCCESS
});

export const refreshTokenFailure = (error) => ({
    payload: {error},
    type: REFRESH_TOKEN_FAILURE
});

export const deployTokenError = () => ({
    type: DEPLOY_TOKEN_ERROR
});

export const eraseTokenError = () => ({
    type: ERASE_TOKEN_ERROR
});
