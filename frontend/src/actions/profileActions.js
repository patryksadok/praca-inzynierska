import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors, handleResponseError} from "./shared";
import {
    getProfileBegin,
    getProfileFailure,
    getProfileSuccess,
    updateProfileBegin,
    updateProfileFailure,
    updateProfileSuccess,
    getOfficialProfileBegin,
    getOfficialProfileFailure,
    getOfficialProfileSuccess,
    getManagerProfileBegin,
    getManagerProfileFailure,
    getManagerProfileSuccess,
    changePasswordBegin,
    changePasswordFailure,
    changePasswordSuccess,
    removeRoleBegin,
    removeRoleFailure,
    removeRoleSuccess,
    getRolesBegin,
    getRolesFailure,
    getRolesSuccess,
    assignRoleBegin,
    assignRoleFailure,
    assignRoleSuccess
} from "./dispatches/profileDispatches";

export const getProfile = () => {
    return (dispatch) => {
        dispatch(getProfileBegin());
        return axios.
            get(`/profile`).
            then((res) => {
                handleErrors(res);
                dispatch(getProfileSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getProfileFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania danych o profilu");
            });
    };
};

export const updateProfile = (data) => {
    return (dispatch) => {
        dispatch(updateProfileBegin());
        return axios.
            patch(`/profile`, {data}).
            then((res) => {
                handleErrors(res);
                dispatch(updateProfileSuccess(res.data));
                toast.success("Pomyślnie zaktualizowano profil");
            }).
            catch((error) => {
                dispatch(updateProfileFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie aktualizacji danych o profilu");
            });
    };
};

export const getOfficialProfile = () => {
    return (dispatch) => {
        dispatch(getOfficialProfileBegin());
        return axios.
            get(`/profile-official`).
            then((res) => {
                handleErrors(res);
                dispatch(getOfficialProfileSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getOfficialProfileFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania danych o profilu");
            });
    };
};

export const getManagerProfile = () => {
    return (dispatch) => {
        dispatch(getManagerProfileBegin());
        return axios.
            get(`/profile-manager`).
            then((res) => {
                handleErrors(res);
                dispatch(getManagerProfileSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getManagerProfileFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania danych o profilu");
            });
    };
};

export const changePassword = (password, newPassword) => {
    const data = {password, newPassword};

    return (dispatch) => {
        dispatch(changePasswordBegin());
        return axios.
            post(`/user/change-password`, {data}).
            then((res) => {
                handleErrors(res);
                dispatch(changePasswordSuccess());
                toast.success('Hasło pomyślnie zmienione');
            }).
            catch((error) => {
                const type = error.response.data.type;
                switch (type) {
                    case 'invalid-login-data':
                        toast.error("Podano nieprawidłowe hasło. Hasło nie zostało zaktualizowane.");
                        break;
                    default:
                        toast.error("Wystąpił błąd podczas próby zmiany hasła");
                }
                dispatch(changePasswordFailure(error.response.data));
            });
    };
};

export const removeRole = (id) => {

    return (dispatch) => {
        dispatch(removeRoleBegin());
        return axios.
            post(`/profile-remove`, {id}).
            then((res) => {
                handleErrors(res);
                dispatch(removeRoleSuccess());
                toast.success('Usunięto profil użytkownika');
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(removeRoleFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie usuwania profilu");
            });
    };
};

export const getRoles = () => {
    return (dispatch) => {
        dispatch(getRolesBegin());
        return axios.
            get(`/profile-roles`).
            then((res) => {
                handleErrors(res);
                dispatch(getRolesSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getRolesFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobierania roli");
            });
    };
};

export const assignRole = (profileId, roleId) => {
    return (dispatch) => {
        dispatch(assignRoleBegin());
        return axios.
            post(`/profile-assign`, {profileId, roleId}).
            then((res) => {
                handleErrors(res);
                dispatch(assignRoleSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(assignRoleFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie ustawiania nowej roli");
            });
    };
};

