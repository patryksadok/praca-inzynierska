import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "../utils/axiosProvider";
import {handleErrors, handleResponseError} from "./shared";
import {
    getUsersBegin,
    getUsersFailure,
    getUsersSuccess
} from './dispatches/usersDispatches';

export const getUsers = () => {
    return (dispatch) => {
        dispatch(getUsersBegin());
        return axios.
            get(`/users-list/`).
            then((res) => {
                handleErrors(res);
                dispatch(getUsersSuccess(res.data));
            }).
            catch((error) => {
                handleResponseError(error);
                dispatch(getUsersFailure(error.response.data));
                toast.error("Wystąpił błąd w trakcie pobieranie");
            });
    };
};
