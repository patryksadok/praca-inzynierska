import {toast} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const devAPI = 'http://localhost:3020';
import {
    refreshTokenBegin,
    refreshTokenFailure,
    refreshTokenSuccess,
    deployTokenError
} from './dispatches/sessionDispatches';

export const handleErrors = (error) => {
    if (error.error) {
        localStorage.removeItem("accessToken");
        throw Error(error.statusText);
    }
    return error;
};

export const handleResponseError = (error) => {
    let type = error.response.data.type;
    console.log(type);

    if (!type) {
        type = error.response.statusText;
    }
    
    switch (type) {
        case "error-profile-not-complete":
            window.location.href = devAPI + "/edit-address";
            break;
        case "Not Found":
            toast.error("Błąd podczas komunikacji z serwerem. Zasób nie istnieje.");
            break;
        case "error-token-expired":
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('userId');
            window.location.href = devAPI;
        default:
            toast.error("nie ma jeszcze reakcji na ten blad, prawdopodobnie to token expired wiec sie przeloguj");
            break;
    }
};

const deployError = () => {
    console.log('test');
    return (dispatch) => {
        dispatch(deployTokenError());
    }
}