'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('attachments', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            description: {
                type: Sequelize.STRING
            },
            confirmed: {
                type: Sequelize.BOOLEAN
            },
            case_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'cases',
                    key: 'id'
                }
            },
            author_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'profiles',
                    key: 'id'
                }
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('attachments');
    }
};
