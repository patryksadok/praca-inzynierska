'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('addresses', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            country: {
                type: Sequelize.STRING(300)
            },
            state: {
                type: Sequelize.STRING(300)
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            city: {
                type: Sequelize.STRING(300)
            },
            street: {
                type: Sequelize.STRING(300)
            },
            home_number: {
                type: Sequelize.STRING(300)
            },
            flat_number: {
                type: Sequelize.STRING(300)
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('addresses');
    }
};
