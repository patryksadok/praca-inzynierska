'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            email: {
                type: Sequelize.STRING(191),
                unique: true
            },
            password: {
                type: Sequelize.STRING(191)
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            user_data_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'userdata',
                    key: 'id'
                }
            },
            active_profile: {
                type: Sequelize.INTEGER
            },
            completed: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                default: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('users');
    }
};
