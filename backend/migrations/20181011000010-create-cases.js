'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('cases', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            closed_at: {
                allowNull: true,
                type: Sequelize.DATE
            },
            description: {
                type: Sequelize.STRING
            },
            awaiting: {
                type: Sequelize.BOOLEAN
            },
            official_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'profiles',
                    key: 'id'
                }
            },
            client_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'profiles',
                    key: 'id'
                }
            },
            verdict_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'verdicts',
                    key: 'id'
                }
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('cases');
    }
};
