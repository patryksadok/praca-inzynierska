module.exports = {
    USER: 'Użytkownik',
    OFFICIAL: 'Urzędnik',
    MANAGER: 'Kierownik',
    ADMIN: 'Admin',
    SUPER_ADMIN: 'Super Admin',
    DEFAULT: 'Default'
};
