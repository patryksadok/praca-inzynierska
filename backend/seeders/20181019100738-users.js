'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
          email: 'disabled@account.pl',
          password: 'aG3Hsd7S!%#gdS',
          user_data_id: '1',
          active_profile: '1',
          completed: '1',
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
      },
      {
        email: 'admin@account.pl',
        password: '$2a$10$s//bLhItecHOS9MnxjqS1utjRhKia.aqL2KXmTlkSuxo5efsxo8z.',
        user_data_id: '2',
        active_profile: '2',
        completed: '1',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        email: 'kierownik@account.pl',
        password: '$2a$10$s//bLhItecHOS9MnxjqS1utjRhKia.aqL2KXmTlkSuxo5efsxo8z.',
        user_data_id: '3',
        active_profile: '3',
        completed: '1',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        email: 'urzednik@account.pl',
        password: '$2a$10$s//bLhItecHOS9MnxjqS1utjRhKia.aqL2KXmTlkSuxo5efsxo8z.',
        user_data_id: '4',
        active_profile: '4',
        completed: '1',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        email: 'petent@account.pl',
        password: '$2a$10$s//bLhItecHOS9MnxjqS1utjRhKia.aqL2KXmTlkSuxo5efsxo8z.',
        user_data_id: '5',
        active_profile: '5',
        completed: '1',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      }
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
