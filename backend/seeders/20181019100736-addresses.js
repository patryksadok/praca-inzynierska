'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('addresses', [
        {
            country: 'Polska',
            state: 'Urząd',
            city: 'Miasto urzędu',
            street: 'Ulica urzędu',
            home_number: 1,
            flat_number: 1,
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
          country: 'Polska',
          state: 'Podkarpackie',
          city: 'Rzeszów',
          street: 'Pigonia',
          home_number: 1,
          flat_number: 14,
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
        },
        {
          country: 'Polska',
          state: 'Podkarpackie',
          city: 'Rzeszów',
          street: 'Podwisłocze',
          home_number: 28,
          flat_number: 145,
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
        },
        {
          country: 'Polska',
          state: 'Podkarpackie',
          city: 'Rzeszów',
          street: 'Podkarpacka',
          home_number: 19,
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
        },
        {
          country: 'Polska',
          state: 'Podkarpackie',
          city: 'Rzeszów',
          street: 'Cicha',
          home_number: 183,
          flat_number: 152,
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
        }
        ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
