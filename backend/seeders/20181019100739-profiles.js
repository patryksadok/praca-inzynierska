'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('profiles', [
      {
          role_id: 2,
          section_id: 1,
          user_id: 1,
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 4,
        section_id: 1,
        user_id: 2,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 3,
        section_id: 1,
        user_id: 3,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 2,
        section_id: 1,
        user_id: 4,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 1,
        section_id: 1,
        user_id: 5,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 1,
        section_id: 1,
        user_id: 4,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 1,
        section_id: 1,
        user_id: 3,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        role_id: 1,
        section_id: 1,
        user_id: 2,
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
