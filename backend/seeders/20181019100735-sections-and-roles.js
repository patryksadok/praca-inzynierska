'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.bulkInsert('sections', [
        {
            type: 'Użytkownik',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Ruch drogowy',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Administracja',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Gospodarka',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Zażalenia',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Dotacje',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        }
    ], {});
    return queryInterface.bulkInsert('roles', [
        {
            type: 'Użytkownik',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Urzędnik',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Kierownik',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Admin',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        },
        {
            type: 'Super Admin',
            created_at: '2018-10-10 10:00:00',
            updated_at: '2018-10-10 10:00:00'
        }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
