'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('userdata', [
      {
          registered_address_id: 1,
          correspondence_address_id: 1,
          first_name: 'Konto',
          last_name: 'Dezaktywowane',
          created_at: '2018-10-10 10:00:00',
          updated_at: '2018-10-10 10:00:00'
      },
      {
        registered_address_id: 2,
        correspondence_address_id: 2,
        first_name: 'Patryk',
        last_name: 'Sadok',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        registered_address_id: 3,
        correspondence_address_id: 3,
        first_name: 'Jan',
        last_name: 'Kowalski',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        registered_address_id: 4,
        correspondence_address_id: 4,
        first_name: 'Adam',
        last_name: 'Nowak',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      },
      {
        registered_address_id: 5,
        correspondence_address_id: 5,
        first_name: 'Józef',
        last_name: 'Kopytko',
        created_at: '2018-10-10 10:00:00',
        updated_at: '2018-10-10 10:00:00'
      }
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
