const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 3003;
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const HttpStatus = require('./config/httpStatus');
const sharedController = require('./controllers/sharedController');
const userManager = require('./managers/userManager');
const attachmentManager = require('./managers/attachmentManager');
const fileSystem = require('fs');
const path = require('path');
const send = require('send');

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use(cors({
    methods: ['GET', 'POST', 'PUT', 'OPTIONS', 'PATCH', 'DELETE'],
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
    credentials: true
}));

app.use(require('./routers/api'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
app.use('/public', express.static(__dirname + '/public'));

app.listen(port, () => {
    console.log('API is running on port: ' + port + '.');
});

app.post('/attachment/:caseId', async (req, res, next) => {
    try {
        const {caseId} = req.params;
        let uploadedFile = req.files.file;
        const description = req.body.filename;
        const userData = await sharedController.getTokenData(req);
        const activeProfileId = await userManager.getActiveProfileId(userData.data.userId);
        await attachmentManager.createAttachment(caseId, description, activeProfileId);
        await uploadedFile.mv(`${__dirname}/public/${req.body.filename}`);

        return res.status(HttpStatus.HTTP_200_OK).json({
            uploadedAttachment: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
});

app.get('/attachment/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        const attachmentName = await attachmentManager.getAttachmentName(id);
        const filePath = await path.join(`${__dirname}/public/`, attachmentName);
        console.log(filePath);
        //const stat = await fileSystem.statSync(filePath);
        //const paths = `./public/${attachmentName}`;
        //send(req, paths).pipe(res);
       //return res.download(`./public/${attachmentName}`);
        res.sendFile(filePath);

       //return res.sendFile(`./public/${attachmentName}`);

        /*await res.writeHead(HttpStatus.HTTP_200_OK, {
            'Content-Type': 'application/pdf',
            'Content-Length': stat.size
        });
        
        const readStream = await fileSystem.createReadStream(filePath);
        await readStream.pipe(res);

        return res.status(HttpStatus.HTTP_200_OK).json({
            uploadedAttachment: true
        }); */
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
})