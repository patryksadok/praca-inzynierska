const HttpStatus = require('../config/httpStatus');

class TokenExpiredError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-token-expired';

        this.message = message ||
            'Token has expired';

        this.status = status || HttpStatus.HTTP_401_UNAUTHORIZED;
    }
}

module.exports = TokenExpiredError;
