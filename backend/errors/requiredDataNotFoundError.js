const HttpStatus = require('../config/httpStatus');

class RequiredDataNotFoundError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-required-data-not-found';

        this.message = message ||
            'Required data not found';

        this.status = status || HttpStatus.HTTP_404_NOT_FOUND;
    }
}

module.exports = RequiredDataNotFoundError;
