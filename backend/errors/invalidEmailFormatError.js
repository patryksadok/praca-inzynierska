const HttpStatus = require('../config/httpStatus');

class InvalidEmailFormatError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'incorrect-email-format';

        this.message = message ||
            'Incorrect email format';

        this.status = status || HttpStatus.HTTP_400_BAD_REQUEST;
    }
}

module.exports = InvalidEmailFormatError;
