const HttpStatus = require('../config/httpStatus');

class InvalidLoginDataError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'user-not-found';

        this.message = message ||
            'User not found in database';

        this.status = status || HttpStatus.HTTP_404_NOT_FOUND;
    }
}

module.exports = InvalidLoginDataError;
