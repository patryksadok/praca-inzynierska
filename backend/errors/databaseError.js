const HttpStatus = require('../config/httpStatus');

class DatabaseError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-wrong-database-sql-syntax';

        this.message = message ||
            'Error in SQL syntax.';

        this.status = status || HttpStatus.HTTP_500_SERVER_FAIL;
    }
}

module.exports = DatabaseError;
