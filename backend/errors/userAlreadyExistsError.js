const HttpStatus = require('../config/httpStatus');

class UserAlreadyExists extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-user-already-exists';

        this.message = message ||
            'User already exists in database';

        this.status = status || HttpStatus.HTTP_404_NOT_FOUND;
    }
}

module.exports = UserAlreadyExists;
