const HttpStatus = require('../config/httpStatus');

class ProfileNotCompleteError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-profile-not-complete';

        this.message = message ||
            'Profile is not complete';

        this.status = status || HttpStatus.HTTP_401_UNAUTHORIZED;
    }
}

module.exports = ProfileNotCompleteError;
