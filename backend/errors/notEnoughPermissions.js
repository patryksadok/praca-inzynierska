const HttpStatus = require('../config/httpStatus');

class ProfileNotCompleteError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'error-not-enough-permissions';

        this.message = message ||
            'Request was not granted enough role permission';

        this.status = status || HttpStatus.HTTP_401_UNAUTHORIZED;
    }
}

module.exports = ProfileNotCompleteError;
