const HttpStatus = require('../config/httpStatus');

class InvalidTokenError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'invalid-token';

        this.message = message ||
            'Invalid token';

        this.status = status || HttpStatus.HTTP_400_BAD_REQUEST;
    }
}

module.exports = InvalidTokenError;
