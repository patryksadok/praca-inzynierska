const HttpStatus = require('../config/httpStatus');

class InvalidLoginDataError extends Error {

    constructor(message, status, type) {
        super();

        Error.captureStackTrace(this, this.constructor);

        this.name = this.constructor.name;

        this.type = type || 'invalid-login-data';

        this.message = message ||
            'Invalid password';

        this.status = status || HttpStatus.HTTP_400_BAD_REQUEST;
    }
}

module.exports = InvalidLoginDataError;
