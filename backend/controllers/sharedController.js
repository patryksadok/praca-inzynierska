const jwt = require('jsonwebtoken');
const TokenExpiredError = require('../errors/tokenExpiredError');

module.exports.getTokenData = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        let userData;
        const decoded = await jwt.verify(token, 'secret', (err, decoded) => {
            if (err) {
                throw new TokenExpiredError();
            }
            userData = decoded
        });
        return userData;
    } catch (error) {
        console.log(error);
    }
};