const HttpStatus = require('../config/httpStatus');
const attachmentManager = require('../managers/attachmentManager');

module.exports.uploadAttachment = async (req, res, next) => {
    try {
        let uploadedFile = req.files.file;
        uploadedFile.mv(`${__dirname}/public/${req.body.filename}`);

        return res.status(HttpStatus.HTTP_200_OK).json({
            uploadedAttachment: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.acceptAttachment = async (req, res) => {
    try {
        const {id} = req.body;

        await attachmentManager.acceptAttachment(id);

        return res.status(HttpStatus.HTTP_200_OK).json({
            acceptedAttachment: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};