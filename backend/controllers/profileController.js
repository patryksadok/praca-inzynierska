const HttpStatus = require('../config/httpStatus');
const sharedController = require('./sharedController');
const userManager = require('../managers/userManager');
const userdataManager = require('../managers/userDataManager');
const profileManager = require('../managers/profileManager');

module.exports.getProfileData = async (req, res) => { 
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const userEmail = await userManager.getUserEmail(userId);
        const userDataId = await userManager.getUserDataFromUserId(userId);
        const namesData = await userdataManager.getUserNames(userDataId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            email: userEmail,
            firstName: namesData.names.firstName,
            lastName: namesData.names.lastName
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};

module.exports.updateProfile = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const updatedEmail = await userManager.updateUserEmail(userId, req.body.data.email);
        const userDataId = await userManager.getUserDataFromUserId(userId);
        const updatedNames = await userdataManager.updateUserNames(userDataId, req.body.data.firstName, req.body.data.lastName);

        return res.status(HttpStatus.HTTP_200_OK).json({
            updatedUserData: updatedNames,
            updatedEmail: updatedEmail
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};

module.exports.getOfficialProfileData = async (req, res) => { 
    try {
        const userData = await sharedController.getTokenData(req);
        const userEmail = await userManager.getUserEmail(userData.data.userId);
        const userDataId = await userManager.getUserDataFromUserId(userData.data.userId);
        const namesData = await userdataManager.getUserNames(userDataId);
        const profileId = await userManager.getActiveProfileId(userData.data.userId);
        const sectionName = await profileManager.getSectionName(profileId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            email: userEmail,
            firstName: namesData.names.firstName,
            lastName: namesData.names.lastName,
            sectionName
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};

module.exports.removeProfile = async (req, res) => {
    try {
        const profileId = req.body.id;

        await profileManager.removeProfile(profileId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            removed: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};

module.exports.getRoles = async (req, res) => {
    try {
        const roles = await profileManager.getRoles();

        return res.status(HttpStatus.HTTP_200_OK).json({
            roles
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};

module.exports.assignRole = async (req, res) => {
    try {
        const {profileId, roleId} = req.body;
        const userId = await userManager.getUserByProfileId(profileId);
        await profileManager.createProfileWithRole(userId, roleId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            created: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    };
};