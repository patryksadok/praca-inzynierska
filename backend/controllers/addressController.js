const HttpStatus = require('../config/httpStatus');
const sharedController = require('./sharedController');
const userManager = require('../managers/userManager');
const addressManager = require('../managers/addressManager');

module.exports.updateCorrespondenceAddress = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const userDataId = await userManager.getUserDataFromUserId(userId);
        const correspondenceAddressId = await addressManager.getCorrespodenceAddressId(userDataId);
        const updatedAddress = await addressManager.updateAddress(correspondenceAddressId, req.body.data);
        await userManager.setUserComplete(userId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            updatedAddress: updatedAddress
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.updateCheckedAddress = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const userDataId = await userManager.getUserDataFromUserId(userId);
        const checkedAddressId = await addressManager.getCheckedAddressId(userDataId);
        const updatedAddress = await addressManager.updateAddress(checkedAddressId, req.body.data);
        await userManager.setUserComplete(userId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            updatedAddress: updatedAddress
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getAddressesForUser = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const userDataId = await userManager.getUserDataFromUserId(userId);
        const checkedAddressId = await addressManager.getCheckedAddressId(userDataId);
        const correspondenceAddressId = await addressManager.getCorrespodenceAddressId(userDataId);
        const checkedAddress = await addressManager.getAddressById(checkedAddressId);
        const correspondenceAddress = await addressManager.getAddressById(correspondenceAddressId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            checkedAddress: checkedAddress,
            correspondenceAddress: correspondenceAddress
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
}