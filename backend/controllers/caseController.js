const HttpStatus = require('../config/httpStatus');
const sharedController = require('./sharedController');
const userManager = require('../managers/userManager');
const caseManager = require('../managers/caseManager');
const profileManager = require('../managers/profileManager');
const attachmentManager = require('../managers/attachmentManager');
const commentManager = require('../managers/commentsManager');
const verdictManager = require('../managers/verdictManager');

module.exports.getUserCases = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const activeProfileId = await userManager.getActiveProfileId(userId);
        const cases = await caseManager.getCasesListForClient(activeProfileId);
        const casesArray = [];
        await cases.forEach((item) => {
            casesArray.push(item.dataValues);
        });

        return res.status(HttpStatus.HTTP_200_OK).json({
            cases: casesArray
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getUserCaseView = async (req, res) => {
    try {
        const attachmentsArray = [];
        const commentsArray = [];
        const { id } = req.params;

        const caseDetails = await caseManager.getCaseDetailsForUser(id);
        const {officialId, verdictId} = caseDetails;

        let officialNames = {};

        if (officialId) {
            officialNames = await profileManager.getNamesOfProfile(officialId);
        }

        const attachments = await attachmentManager.getAttachmentsForCase(id);
        await attachments.forEach((item) => {
            attachmentsArray.push(item.dataValues);
        });

        const comments = await commentManager.getCommentsForCase(id);

        await comments.forEach(async (item) => {
            await commentsArray.push(item);
        });
        
        const verdict = await verdictManager.getVerdictDetails(verdictId);
        const caseData = {caseDetails, officialNames, attachmentsArray, comments, verdict};
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            caseData: caseData
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.closeCase = async (req, res) => {
    try {
        const { id } = req.params;
        await caseManager.closeCase(id);

        return res.status(HttpStatus.HTTP_200_OK).json({
            caseClosed: 'true'
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.createCase = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const activeProfileId = await userManager.getActiveProfileId(userId);
        const description = req.body.description;
        await caseManager.createCase(activeProfileId, description);
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            caseCreated: 'true'
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getOfficialCases = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const activeProfileId = await userManager.getActiveProfileId(userData.data.userId);
        const cases = await caseManager.getCasesListForOfficial(activeProfileId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            cases
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getOfficialCaseView = async (req, res) => {
    try {
        const attachmentsArray = [];
        const commentsArray = [];
        const { id } = req.params;

        const caseDetails = await caseManager.getCaseDetailsForUser(id);
        const {clientId, verdictId} = caseDetails;

        let clientNames = {};

        if (clientId) {
            clientNames = await profileManager.getNamesOfProfile(clientId);
        }

        const attachments = await attachmentManager.getAttachmentsForCase(id);
        await attachments.forEach((item) => {
            attachmentsArray.push(item.dataValues);
        });

        const comments = await commentManager.getCommentsForCase(id);
        await comments.forEach(async (item) => {
            commentsArray.push(item.dataValues);
        });

        const verdict = await verdictManager.getVerdictDetails(verdictId);
        const caseData = {caseDetails, clientNames, attachmentsArray, comments, verdict};
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            caseData: caseData
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.confirmCase = async (req, res) => {
    try {
        const { id } = req.params;
        await caseManager.confirmCase(id);

        return res.status(HttpStatus.HTTP_200_OK).json({
            caseConfirmed: 'true'
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getManagerCases = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const activeProfileId = await userManager.getActiveProfileId(userData.data.userId);
        const cases = await caseManager.getCasesListForManager(activeProfileId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            cases
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getManagerCaseView = async (req, res) => {
    try {
        const { id } = req.params;

        const caseDetails = await caseManager.getCaseDetailsForUser(id);
        const {clientId, verdictId, officialId} = caseDetails;
        let clientNames = {}, officialNames = {};

        if (clientId) {
            clientNames = await profileManager.getNamesOfProfile(clientId);
        }

        if (officialId) {
            officialNames = await profileManager.getNamesOfProfile(officialId);
        }

        const verdict = await verdictManager.getVerdictDetails(verdictId);
        const caseData = {caseDetails, clientNames, verdict, officialNames};
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            caseData: caseData
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getOfficialsList = async (req, res) => {
    try {
        const officialsList = await caseManager.getOfficialsList();
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            officialsList
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.assignOfficialToCase = async (req, res) => {
    try {
        const {id, officialId} = req.body;
        await caseManager.assignOfficialToCase(officialId, id);
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            created: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};