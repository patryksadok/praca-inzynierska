const HttpStatus = require('../config/httpStatus');
const sharedController = require('./sharedController');
const userManager = require('../managers/userManager');
const commentManager = require('../managers/commentsManager');
const verdictManager = require('../managers/verdictManager');

module.exports.addComment = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const {comment} = req.body;
        const {id} = req.params;
        const activeProfileId = await userManager.getActiveProfileId(userData.data.userId);
        await commentManager.addComment(id, activeProfileId, comment);

        return res.status(HttpStatus.HTTP_200_OK).json({
            addedComment: true
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};
