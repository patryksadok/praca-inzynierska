const RequiredDataNotFoundError = require('../errors/requiredDataNotFoundError');
const userManager = require('../managers/userManager');
const tokenManager = require('../managers/tokenManager');
const addressManager = require('../managers/addressManager');
const userDataManager = require('../managers/userDataManager');
const profileManager = require('../managers/profileManager');
const HttpStatus = require('../config/httpStatus');
const invalidEmailFormatError = require('../errors/invalidEmailFormatError');
const invalidLoginDataError = require('../errors/invalidLoginDataError');
const UserAlreadyExistsError = require('../errors/userAlreadyExistsError');
const sharedController = require('./sharedController');

module.exports.register = async (req, res) => {
    const {email, password} = req.body;
    try {
        if (!email || !password) {
            throw new RequiredDataNotFoundError();
        }
        const user =  await userManager.checkIfUserExist(email);
        if (user) {
            throw new UserAlreadyExistsError();
        }
        const addressOne = await addressManager.createAddress();
        const addressTwo = await addressManager.createAddress();
        const userData = await userDataManager.createNewUserData(addressOne.dataValues.id, addressTwo.dataValues.id);
        const createdUser = await userManager.createUser(email, password, userData.dataValues.id);
        const createdUserProfile = await profileManager.createUserProfile(createdUser.dataValues.id);
        await userManager.setUserProfile(createdUser.dataValues.id, createdUserProfile.dataValues.id);

        return res.status(HttpStatus.HTTP_200_OK).json({
            emailLogin: createdUser.emailLogin
        });
    } catch (error) {
        console.log(error);
        console.log(error.status);
        console.log(error.message);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.login = async (req, res) => {
    const {email, password} = req.body;
    try {
        if (!email || !password) {
            throw new RequiredDataNotFoundError();
        }
        if (!(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,15}$/i).test(email)) {
            throw new invalidEmailFormatError();
        }
        if (password.length < 8) {
            throw new invalidLoginDataError();
        }

        const foundUserId = await userManager.validateUser(email, password);

        await tokenManager.createToken(email, foundUserId);
        const tokens = await tokenManager.getTokens(foundUserId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            userId: foundUserId,
            accessToken: tokens.dataValues.accessToken,
            refreshToken: tokens.dataValues.refreshToken
        });

    } catch (error) {
        console.log(error);
        console.log(error.status);
        console.log(error.message);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.logout = async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        await tokenManager.destroyToken(token);

        return res.status(HttpStatus.HTTP_200_OK).json({
            text: "Succesfully logged out"
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.isUserComplete = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        isUserComplete = await userManager.isUserComplete(userId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            complete: isUserComplete
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.changePassword = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const userId = userData.data.userId;
        const {password, newPassword} = req.body.data;
        
        await userManager.changePassword(userId, password, newPassword);

        return res.status(HttpStatus.HTTP_200_OK).json({
            changedPassword: 'true'
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};


module.exports.getUsersList = async (req, res) => {
    try {
        const users = await userManager.findUsers("Użytkownik");
        const officials = await userManager.findUsers("Urzędnik");
        const managers = await userManager.findUsers("Kierownik");

        const usersList = {users, officials, managers};
        
        return res.status(HttpStatus.HTTP_200_OK).json({
            usersList
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};