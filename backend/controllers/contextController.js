const HttpStatus = require('../config/httpStatus');
const sharedController = require('./sharedController');
const profileManager = require('../managers/profileManager');
const userManager = require('../managers/userManager');

module.exports.getContextList = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const profiles = await profileManager.getAvaibleProfiles(userData.data.userId);
        const activeProfile = await userManager.getActiveProfileId(userData.data.userId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            profiles,
            activeProfile
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.getCurrentContext = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const activeContextRole = await profileManager.getActiveProfileRole(userData.data.userId);

        return res.status(HttpStatus.HTTP_200_OK).json({
            activeRole: activeContextRole
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};

module.exports.changeCurrentContext = async (req, res) => {
    try {
        const userData = await sharedController.getTokenData(req);
        const contextId = req.body.contextId;
        await userManager.setUserProfile(userData.data.userId, contextId);
        
        const activeRole = await profileManager.getActiveProfileRole(userData.data.userId);
        return res.status(HttpStatus.HTTP_200_OK).json({
            activeRole: activeRole
        });
    } catch (error) {
        console.log(error);
        return res.status(error.status).json({
            message: error.message,
            type: error.type,
        });
    }
};