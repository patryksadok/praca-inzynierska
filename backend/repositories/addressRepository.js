const Address = require('../models').addresses;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
dotenv.config({'path': '.env'});

module.exports.createNewAddress = async () => {
    return await Address.create().catch(error => {
        console.log(error);
        throw new databaseError("Error while creating address" + error.message);
    });
};

module.exports.updateAddress = async (id, data) => {
    const {country, state, city, street, homeNumber, flatNumber} = data;

    return await Address.update({
        country,
        state,
        city,
        street,
        homeNumber,
        flatNumber
    }, {
        where: {
            id
        }
    }).catch(error => {
        throw new DatabaseError(error.message);
    });
};

module.exports.getAddressById = async (id) => {
    return await Address.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};