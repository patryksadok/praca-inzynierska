const Case = require('../models').cases;
const sequelize = require('../models').sequelize;
const databaseError = require('../errors/databaseError');
const moment = require('moment');

module.exports.getCasesListForClient = async (clientId) => {
    const cases =  await Case.findAll({
        where: {
            clientId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return cases;
};

module.exports.getCasesListForOfficial = async (officialId) => {
    const cases =  await Case.findAll({
        where: {
            officialId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return cases;
};

module.exports.getCaseDetails = async (id) => {
    return await Case.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
};

module.exports.closeCase = async (id) => {
    const dateNow = moment().format();
    
    return await Case.update({
        closedAt: dateNow,
        awaiting: false
    }, 
    {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
};

module.exports.createCase = async (clientId, description) => {
    const dateNow = moment().format();

    return await Case.create({
        createdAt: dateNow,
        updatedAt: dateNow,
        description,
        clientId,
        awaiting: true
    });
};

module.exports.confirmCase = async (id) => {

    return await Case.update({
        awaiting: false
    }, 
    {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
};

module.exports.getCasesListForManager = async () => {
    const cases =  await Case.findAll().catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return cases;
};

module.exports.getOfficialsList = async () => {
    let officialsList = await sequelize.query('SELECT p.id, ud.first_name AS firstName, ud.last_name AS lastName ' +
        'FROM profiles p ' +
        ' JOIN userdata ud ON p.user_id = ud.id ' +
        'AND p.role_id = 2');
    officialsList = officialsList[0];

    return officialsList;
};

module.exports.assignOfficialToCase = async (id, officialId) => {
    return await Case.update({
        officialId
    }, 
    {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
};

module.exports.resetOfficialCases = async (officialId) => {
    return await Case.update({
        officialId: null
    }, 
    {
        where: {
            officialId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
};