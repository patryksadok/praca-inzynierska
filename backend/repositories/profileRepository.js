const Profile = require('../models').profiles;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
dotenv.config({'path': '.env'});

module.exports.createNewUserProfile = async (userId) => {
    const roleId = 1;
    const sectionId = 1;
    return await Profile.create({
        userId,
        roleId,
        sectionId
    }).catch(error => {
        throw new databaseError("Error while creating new default profile" + error.message);
    });
};
 
module.exports.getProfileByUserId = async (userId) => {
    return await Profile.findOne({
        where: {
            userId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.getProfileById = async (id) => {
    return await Profile.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.getAvaibleProfiles = async (userId) => {
    const profiles =  await Profile.findAll({
        where: {
            userId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return profiles;
};

module.exports.removeProfile = async (id) => {
    return await Profile.destroy({
        where: {
            id
        }
    });
};

module.exports.getUserIdByProfileId = async (id) => {
    const profile = await Profile.findOne({
        where: {
            id
        }
    });
    return profile.dataValues.userId;
};

module.exports.createProfileWithRole = async (userId, roleId) => {
    const sectionId = 1;
    return await Profile.create({
        userId,
        roleId,
        sectionId
    }).catch(error => {
        throw new databaseError("Error while creating new default profile" + error.message);
    });
};
