const Verdict = require('../models').verdict;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
dotenv.config({'path': '.env'});

module.exports.getVerdictById = async (id) => {
    return await Verdict.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};
