const Comment = require('../models').comments;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
dotenv.config({'path': '.env'});

module.exports.getCommentsForCase = async (caseId) => {
    const comments =  await Comment.findAll({
        where: {
            caseId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return comments;
};

module.exports.addComment = async (caseId, authorId, value) => {
    return await Comment.create({
        caseId,
        authorId,
        value
    });
};

module.exports.changeAuthor = async (authorId) => {

    return await Comment.update({
        authorId: 1
    }, {
        where: {
            authorId
        }
    }).catch(error => {
        throw new DatabaseError(error.message);
    });
};
