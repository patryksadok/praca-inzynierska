const Attachment = require('../models').attachments;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
const moment = require('moment');

dotenv.config({'path': '.env'});

module.exports.getAttachmentsForCase = async (caseId) => {
    const attachments =  await Attachment.findAll({
        where: {
            caseId
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return attachments;
};

module.exports.createAttachment = async (caseId, description, authorId) => {
    const dateNow = moment().format();

    return await Attachment.create({
        createdAt: dateNow,
        updatedAt: dateNow,
        authorId,
        description,
        caseId,
        confirmed: false
    });
};

module.exports.getAttachmentName = async (id) => {
    const attachment =  await Attachment.find({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    
    return attachment.dataValues.description;
};

module.exports.acceptAttachment = async (id) => {

    return await Attachment.update({
        confirmed: true
    }, {
        where: {
            id
        }
    }).catch(error => {
        throw new DatabaseError(error.message);
    });
};

module.exports.changeAuthor = async (authorId) => {

    return await Attachment.update({
        authorId: 1
    }, {
        where: {
            authorId
        }
    }).catch(error => {
        throw new DatabaseError(error.message);
    });
};
