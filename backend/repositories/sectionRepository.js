const Section = require('../models').sections;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
dotenv.config({'path': '.env'});
 
module.exports.getSectionById = async (id) => {
    return await Section.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};
