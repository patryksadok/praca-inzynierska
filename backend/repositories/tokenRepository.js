const Token = require('../models').tokens;

module.exports.findTokensByUserId = async (userId) => {
    return await Token.findOne({
        where: {
            userId
        }
    });
};

module.exports.deleteTokenById = async (id) => {
    return await Token.destroy({
        where: {
            id
        }
    });
};

module.exports.createToken = async (accessToken, refreshToken, userId) => {
    return await Token.create({
        accessToken,
        refreshToken,
        userId
    }).catch(error => {
        throw new databaseError("Error while creating token " + error.message);
    });
}

module.exports.updateToken = async (accessToken, refreshToken, userId) => {

    return await Token.update({
        accessToken,
        refreshToken
    }, {
        where: {
            userId
        }
    }).catch(error => {
        throw new DatabaseError(error.message);
    });
}
