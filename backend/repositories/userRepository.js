const User = require('../models').users;
const databaseError = require('../errors/databaseError');
const invalidEmailFormatError = require('../errors/invalidEmailFormatError');
const invalidLoginDataError = require('../errors/invalidLoginDataError');
const dotenv = require('dotenv');
const rolesRepository = require('./rolesRepository');
const sequelize = require('../models').sequelize;
dotenv.config({'path': '.env'});

module.exports.getUserByEmail = async (email) => {
    const user = await User.findOne({ where: {email} });

    return user;
};

module.exports.getUserById = async (id) => {
    return await User.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.createUser = async (email, password, userDataId) => {
    if (!(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,15}$/i).test(email)) {
        throw new invalidEmailFormatError();
    }
    if (password.length < 8) {
        throw new invalidLoginDataError();
    }
    return await User.create({
        email,
        password,
        userDataId
    }).catch(error => {
        console.log(error);
        throw new databaseError("Error while creating user " + error.message);
    });
};

module.exports.setUserProfile = async (id, profileId) => {
    return await User.update({
        activeProfile: profileId,
      }, {
        where: {
            id
        }
      }).catch(error => {
        console.log(error);
        throw new databaseError("Error while updating user " + error.message);
    });
};

module.exports.setUserComplete = async (id) => {
    return await User.update({
        completed: true
    }, { 
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError("Error while updating user " + error.message);
    });
};

module.exports.updateUserEmail = async (id, email) => {
    return await User.update({
        email
    }, {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError("Error while updating user " + error.message);
    });
};

module.exports.isUserComplete = async (id) => {
    const user =  await User.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    });
    const isComplete = await user.dataValues.completed;
    
    return isComplete;
};

module.exports.updatePassword = async (id, password) => {
    return await User.update({
        password
    }, {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError("Error while updating user " + error.message);
    });
};

module.exports.findUsers  = async (role) => {
    const roleData = await rolesRepository.getRoleByName(role);
    const roleId = await roleData.dataValues.id;

    let usersList = await sequelize.query(
        'SELECT p.id, ud.first_name AS firstName, ud.last_name AS lastName ' +
        'FROM profiles p ' +
        'JOIN userdata ud on p.user_id = ud.id ' +
        'WHERE p.role_id = ' + roleId,
        {
            replacements: {
                roleId
            },
            type: sequelize.QueryTypes.SELECT
        }

    );
    
    return usersList;
};
