const Userdata = require('../models').userdata;
const databaseError = require('../errors/databaseError');

module.exports.createNewUserdata = async (registeredAddressId, correspondenceAddressId) => {

    return await Userdata.create({
        registeredAddressId,
        correspondenceAddressId
    }).catch(error => {
        throw new databaseError("Error while creating token " + error.message);
    });
};

module.exports.getUserDataById = async (id) => {
    return await Userdata.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.updateUserNames = async (id, firstName, lastName) => {
    return await Userdata.update({
        firstName,
        lastName
    }, {
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError("Error while updating user " + error.message);
    });
};
