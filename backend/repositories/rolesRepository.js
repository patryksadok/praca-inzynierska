const Role = require('../models').roles;
const databaseError = require('../errors/databaseError');
const dotenv = require('dotenv');
const sequelize = require('../models').sequelize;

dotenv.config({'path': '.env'});
 
module.exports.getRoleById = async (id) => {
    return await Role.findOne({
        where: {
            id
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.getAllRoles = async () => {
    return await Role.findAll().catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.getRoleByName = async (type) => {
    return await Role.findOne({
        where: {
            type
        }
    }).catch(error => {
        console.log(error);
        throw new databaseError(error.message);
    })
};

module.exports.getRoles = async () => {
    let roles = await sequelize.query('SELECT r.id, r.type ' +
        'FROM roles r ' +
        'WHERE r.id > 1 AND r.id < 4');
        roles = roles[0];

    return roles;
};

