const commentRepository = require('../repositories/commentRepository');
const databaseError = require('../errors/databaseError');
const profileManager = require('./profileManager');

module.exports.getCommentsForCase = async (caseId) => {
    try {
        const comments = await commentRepository.getCommentsForCase(caseId);
        const commentsArray = [];
        await comments.forEach(async (item) => {
            await commentsArray.push(item.dataValues);
        });
        
        return commentsArray;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.addComment = async (caseId, authorId, value) => {
    try {
        return await commentRepository.addComment(caseId, authorId, value);
    } catch (error) {
        throw new databaseError(error.message);
    }
}