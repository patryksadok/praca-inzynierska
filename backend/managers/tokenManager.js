const tokenRepository = require('../repositories/tokenRepository');
const databaseError = require('../errors/databaseError');
const invalidTokenError = require('../errors/invalidTokenError');
const jwt = require('jsonwebtoken');

module.exports.createToken = async (email, userId) => {
    try {
        let token;
        const accessToken = await this.generateAccessToken(userId, email);
        const refreshToken = await this.generateRefreshToken(userId, email);
        const foundToken = await tokenRepository.findTokensByUserId(userId);

        if(!foundToken) {
            token = await tokenRepository.createToken(accessToken, refreshToken, userId);
            
            return token;
        }
        else {
            token = await this.updateToken(accessToken, refreshToken, userId);

            return token;
        }
    }
    catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.getTokens = async (userId) => {
    return await tokenRepository.findTokensByUserId(userId);
};

module.exports.updateToken = async (accessToken, refreshToken, userId) => {
    try {
        const token = await tokenRepository.updateToken(accessToken, refreshToken, userId);

        return token;
    }
    catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.generateAccessToken = async (userId, email) => {
    tokenData = {
        userId,
        email
    };

    return await jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        data: tokenData
    }, 'secret');
};

module.exports.generateRefreshToken = async (userId, email) => {
    tokenData = {
        userId,
        email
    };

    return await jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
        data: tokenData
    }, 'secret');
};

module.exports.destroyToken = async (accessToken) => {
    const decoded = await jwt.decode(accessToken);
    if (!decoded || !decoded.data || !decoded.data.userId) {
        throw new invalidTokenError();
    }
    try {
        const token = await tokenRepository.findTokensByUserId(decoded.data.userId);
        await tokenRepository.deleteTokenById(token.dataValues.id);
    } catch (error) {
        console.log("Error caught. Session was enabled only on frontend. Destroyed frontend session.");
    }
};
