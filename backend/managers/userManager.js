const userRepository = require('../repositories/userRepository');
const userAlreadyExistsError = require('../errors/userAlreadyExistsError');
const userNotFoundError = require('../errors/userNotFoundError');
const invalidLoginDataError = require('../errors/invalidLoginDataError');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
const databaseError = require('../errors/databaseError');
const sequelize = require('../models').sequelize;
const profileRepository = require('../repositories/profileRepository');

module.exports.checkIfUserExist = async (email) => {
    const userFromDB = await userRepository.getUserByEmail(email);

    if (userFromDB) {

        return userFromDB;
    }

    return null;
};

module.exports.findUser = async (email) => {
    const userFromDB = await userRepository.getUserByEmail(email);

    if (!userFromDB) {
        throw new userNotFoundError();
    }

    return userFromDB;
}

module.exports.getUserEmail  = async (id) => {
    const user = await userRepository.getUserById(id);
    return  user.dataValues.email;
}

module.exports.createUser = async (email, password, userDataId) => {
    try {
        const encryptedPassword = await bcrypt.hashSync(password, salt);
        const createdUser = await userRepository.createUser(email, encryptedPassword, userDataId);

        return createdUser;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.validateUser = async (email, password) => {
    const foundUser = await userRepository.getUserByEmail(email);
    if (!foundUser) {
        throw new userNotFoundError();
    }
    const dbEmail = foundUser.dataValues.email;
    const dbPassword = foundUser.dataValues.password;

    if (dbEmail!==email || !bcrypt.compareSync(password, dbPassword)) {
        throw new invalidLoginDataError();
    }

    return foundUser.dataValues.id;
};

module.exports.setUserProfile = async (id, profileId) => {
    try {
        const updatedUser = await userRepository.setUserProfile(id, profileId);

        return updatedUser;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.isUserComplete = async (id) => {
    return await userRepository.isUserComplete(id);
};

module.exports.getUserDataFromUserId = async (id) => {
    const user = await userRepository.getUserById(id);
    return user.dataValues.userDataId;
};

module.exports.updateUserEmail = async (id, email) => {
    return await userRepository.updateUserEmail(id, email);
};

module.exports.setUserComplete = async (id) => {
    return await userRepository.setUserComplete(id);
};

module.exports.getActiveProfileId = async (id) => {
    const user = await userRepository.getUserById(id);
    return user.dataValues.activeProfile;
};

module.exports.changePassword = async (id, password, newPassword)  => {
    const user = await userRepository.getUserById(id);
    const oldPassword = user.dataValues.password;

    if (!bcrypt.compareSync(password, oldPassword)) {
        throw new invalidLoginDataError();
    }
    const encryptedPassword = await bcrypt.hashSync(password, salt);
    return await userRepository.updatePassword(id, encryptedPassword);
};

module.exports.findUsers  = async (role) => {
    const users = await userRepository.findUsers(role);
    return users;
};

module.exports.getUserByProfileId = async (id) => {
    return await profileRepository.getUserIdByProfileId(id);
};
