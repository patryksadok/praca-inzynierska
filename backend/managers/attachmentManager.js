const attachmentRepository = require('../repositories/attachmentRepository');
const databaseError = require('../errors/databaseError');

module.exports.getAttachmentsForCase = async (caseId) => {
    try {
        const attachments = await attachmentRepository.getAttachmentsForCase(caseId);

        return attachments;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.createAttachment = async (caseId, description, authorId) => {
    try {
        return await attachmentRepository.createAttachment(caseId, description, authorId);
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.getAttachmentName = async (id) => {
    try {
        return await attachmentRepository.getAttachmentName(id);
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.acceptAttachment = async (id) => {
    try {
        return await attachmentRepository.acceptAttachment(id);
    } catch (error) {
        throw new databaseError(error.message);
    }
}