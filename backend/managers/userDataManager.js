const userDataRepository = require('../repositories/userDataRepository');
const DatabaseError = require('../errors/databaseError');
const sequelize = require('../models').sequelize;

module.exports.createNewUserData = async (registeredAddressId, correspondenceAddressId) => {
    try {
        const userData = userDataRepository.createNewUserdata(registeredAddressId, correspondenceAddressId);

        return userData;
    } catch (error) {
        throw new DatabaseError(error.message);
    }
};

module.exports.getUserNames = async (id) => {
    try {
        const userData = await userDataRepository.getUserDataById(id);
        const firstName = userData.dataValues.firstName;
        const lastName = userData.dataValues.lastName;
        const names =  {firstName, lastName};
        
        return {names};
    } catch (error) {
        throw new DatabaseError(error.message);
    }
};

module.exports.updateUserNames = async (id, firstName, lastName) => {
    return await userDataRepository.updateUserNames(id, firstName, lastName);
};
