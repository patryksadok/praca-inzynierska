const databaseError = require('../errors/databaseError');
const caseRepository = require('../repositories/caseRepository');

module.exports.getCasesListForClient = async (clientId) => {
    return await caseRepository.getCasesListForClient(clientId);
};

module.exports.getCasesListForOfficial = async (officialId) => {
    const cases = await caseRepository.getCasesListForOfficial(officialId);
    let awaitingCases = [], closedCases = [], inprogressCases = [];

    cases.forEach(element => {
        if (element.dataValues.awaiting) {
            awaitingCases.push(element);
        }
        if (!element.dataValues.awaiting && !element.dataValues.closedAt) {
            inprogressCases.push(element);
        }
        if (!element.dataValues.awaiting && element.dataValues.closedAt) {
            closedCases.push(element);
        }
    });

    casesData = {
        awaitingCases,
        inprogressCases,
        closedCases
    };

    return casesData;
};

module.exports.getCaseDetailsForUser = async (id) => {
    const foundCase = await caseRepository.getCaseDetails(id);
    const {createdAt, updatedAt, closedAt, description, awaiting, verdictId, officialId, clientId} = foundCase.dataValues;
    const data = {createdAt,  updatedAt, closedAt, description, awaiting, verdictId, officialId, clientId};
    
    return await data;
};

module.exports.closeCase = async (id) => {
    return await caseRepository.closeCase(id);
};

module.exports.createCase = async (clientId, description) => {
    return await caseRepository.createCase(clientId, description);
};

module.exports.confirmCase = async (id) => {
    return await caseRepository.confirmCase(id);
};

module.exports.getOfficialsList = async () => {
    return await caseRepository.getOfficialsList();
};

module.exports.getCasesListForManager = async () => {
    const cases = await caseRepository.getCasesListForManager();
    let awaitingCases = [], closedCases = [], inprogressCases = [], awaitingToAssignCases = [];

    cases.forEach(element => {
        if (element.dataValues.awaiting && !element.dataValues.officialId) {
            awaitingToAssignCases.push(element);
        }
        if (element.dataValues.awaiting && element.dataValues.officialId) {
            awaitingCases.push(element);
        }
        if (!element.dataValues.awaiting && !element.dataValues.closedAt) {
            inprogressCases.push(element);
        }
        if (!element.dataValues.awaiting && element.dataValues.closedAt) {
            closedCases.push(element);
        }
    });

    casesData = {
        awaitingCases,
        inprogressCases,
        closedCases,
        awaitingToAssignCases
    };

    return casesData;
};

module.exports.assignOfficialToCase = async (officialId, id) => {
    return await caseRepository.assignOfficialToCase(officialId, id);
};