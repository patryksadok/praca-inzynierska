const verdictRepository = require('../repositories/verdictRepository');
const databaseError = require('../errors/databaseError');

module.exports.getVerdictDetails = async (id) => {
    try {
        if (!id) {
            return null;
        }
        const verdict = await verdictRepository.getVerdictById(id);
        const {createdAt, updatedAt, isConfirmed, validationTime, descripton} = verdict;
        const verdictData = {createdAt, updatedAt, isConfirmed, validationTime, descripton}
        return verdictData;
    } catch (error) {
        throw new databaseError(error.message);
    }
};
