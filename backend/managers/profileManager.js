const profileRepository = require('../repositories/profileRepository');
const databaseError = require('../errors/databaseError');
const sequelize = require('../models').sequelize;
const userRepository = require('../repositories/userRepository');
const rolesRepository = require('../repositories/rolesRepository');
const userdataManager = require('../managers/userDataManager');
const sectionRepository = require('../repositories/sectionRepository');
const caseRepository = require('../repositories/caseRepository');
const attachmentRepository = require('../repositories/attachmentRepository');
const commentRepository = require('../repositories/commentRepository');

module.exports.createUserProfile = async (userId) => {
    try {
        const profile = await profileRepository.createNewUserProfile(userId);

        return profile;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.getActiveProfileRole = async (id) => {
    try {
        user = await userRepository.getUserById(id);
        activeProfile= await user.dataValues.activeProfile;
        profile = await profileRepository.getProfileById(activeProfile);
        roleId = await profile.dataValues.roleId;
        role = await rolesRepository.getRoleById(roleId);

        return role.dataValues.type;
    } catch(error) {
        throw new databaseError(error.message);
    }
};

module.exports.getNamesOfProfile = async (id) => {
    try {
        profile = await profileRepository.getProfileById(id);
        const userId = await profile.dataValues.userId;
        const user = await userRepository.getUserById(userId);
        const userdataId = await user.dataValues.userDataId;
        const names = await userdataManager.getUserNames(userdataId);

        return names;
    } catch(error) {
        throw new databaseError(error.message);
    }
}

module.exports.getAvaibleProfiles = async (userId) => {
    try {
        profiles = await profileRepository.getAvaibleProfiles(userId);
        let profilesData  = [], rolesData = [];

        const rolesList = await rolesRepository.getAllRoles();
        rolesList.forEach(element => {
            rolesData.push(
                {
                    id: element.dataValues.id, 
                    type:element.dataValues.type
                });
        });
        profiles.forEach(element => {
            rolesData.forEach(roleElement => {
                if (element.dataValues.roleId === roleElement.id) {
                    profilesData.push({
                        profile: element.dataValues,
                        roleName: roleElement.type
                    });
                }
            });
        });

        return profilesData;
    } catch(error) {
        throw new databaseError(error.message);
    }
};

module.exports.getSectionName = async (id) => {
    try {
        profile = await profileRepository.getProfileById(activeProfile);
        sectionId = await profile.dataValues.sectionId;
        section = await sectionRepository.getSectionById(sectionId);

        return section.dataValues.type;
    } catch(error) {
        throw new databaseError(error.message);
    }
};

module.exports.removeProfile = async (id) => {
    try {
        await caseRepository.resetOfficialCases(id);
        await attachmentRepository.changeAuthor(id);
        await commentRepository.changeAuthor(id);

        return await profileRepository.removeProfile(id);
    } catch(error) {
        throw new databaseError(error.message);
    }
};

module.exports.getRoles = async () => {
    try {
        const roles = await rolesRepository.getRoles();
        
        return await roles;
    } catch(error) {
        throw new databaseError(error.message);
    }
};

module.exports.createProfileWithRole = async (userId, roleId) => {
    try {
        return await profileRepository.createProfileWithRole(userId, roleId);
    } catch (error) {
        throw new databaseError(error.message);
    }
};