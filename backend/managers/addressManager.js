const addressRepository = require('../repositories/addressRepository');
const userdataRepository = require('../repositories/userDataRepository');
const databaseError = require('../errors/databaseError');
const sequelize = require('../models').sequelize;

module.exports.createAddress = async () => {
    try {
        const address = await addressRepository.createNewAddress();

        return address;
    } catch (error) {
        throw new databaseError(error.message);
    }
};

module.exports.getCorrespodenceAddressId = async (id) => {
    const userData = await userdataRepository.getUserDataById(id);
    return userData.dataValues.correspondenceAddressId;
};

module.exports.getCheckedAddressId = async (id) => {
    const userData = await userdataRepository.getUserDataById(id);
    return userData.dataValues.registeredAddressId;
};

module.exports.updateAddress = async (id, data) => {
    return await addressRepository.updateAddress(id, data);
};

module.exports.getAddressById = async (id) => {
    const address = await addressRepository.getAddressById(id);
    return address.dataValues;
};