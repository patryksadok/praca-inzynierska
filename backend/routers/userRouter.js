const userController = require('../controllers/userController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.put('/user/register',  userController.register);
router.post('/user/login', userController.login);
router.get('/user/logout', hasRole(Role.DEFAULT, false), userController.logout);
router.get('/user/isComplete', hasRole(Role.DEFAULT, false), userController.isUserComplete);
router.post('/user/change-password', hasRole(Role.USER, false), userController.changePassword);
router.get('/users-list', hasRole(Role.ADMIN, true), userController.getUsersList);

module.exports = router;
