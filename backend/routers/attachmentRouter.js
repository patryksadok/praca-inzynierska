const attachmentController = require('../controllers/attachmentController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.post('/attachments', attachmentController.uploadAttachment);
router.post('/accept-attachment/', hasRole(Role.OFFICIAL, true), attachmentController.acceptAttachment);

module.exports = router;