const addressController = require('../controllers/addressController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.patch('/address-correspondence/edit', addressController.updateCorrespondenceAddress);
router.patch('/address-checked/edit', hasRole(Role.DEFAULT, false), addressController.updateCheckedAddress);
router.get('/address', hasRole(Role.DEFAULT, false), addressController.getAddressesForUser);

module.exports = router;
