const express = require('express');
const router = express.Router();

router.use(require('./userRouter'));
router.use(require('./addressRouter'));
router.use(require('./profileRouter'));
router.use(require('./caseRouter'));
router.use(require('./contextRouter'));
router.use(require('./commentRouter'));
router.use(require('./attachmentRouter'));

module.exports = router;
