const caseController = require('../controllers/caseController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.get('/cases-list', hasRole(Role.USER, true), caseController.getUserCases);
router.get('/case/:id', hasRole(Role.USER, true), caseController.getUserCaseView);
router.get('/case-official/:id', hasRole(Role.OFFICIAL, true), caseController.getOfficialCaseView);
router.post('/case/:id', hasRole(Role.USER, true), caseController.closeCase);
router.post('/case-confirm/:id', hasRole(Role.OFFICIAL, true), caseController.confirmCase);
router.post('/case-create', hasRole(Role.USER, true), caseController.createCase);
router.get('/official-cases-list', hasRole(Role.OFFICIAL, true), caseController.getOfficialCases);
router.get('/manager-cases-list', hasRole(Role.MANAGER, true), caseController.getManagerCases);
router.get('/case-manager/:id', hasRole(Role.MANAGER, true), caseController.getManagerCaseView);
router.get('/officials-list', hasRole(Role.MANAGER, true), caseController.getOfficialsList);
router.post('/assign-official', hasRole(Role.OFFICIAL, true), caseController.assignOfficialToCase);

module.exports = router;