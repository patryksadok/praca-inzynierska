const profileController = require('../controllers/profileController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.get('/profile', hasRole(Role.DEFAULT, true), profileController.getProfileData);
router.get('/profile-official', hasRole(Role.OFFICIAL, true), profileController.getOfficialProfileData);
router.patch('/profile', hasRole(Role.DEFAULT, true), profileController.updateProfile);
router.get('/profile-manager', hasRole(Role.MANAGER, true), profileController.getOfficialProfileData);
router.post('/profile-remove', hasRole(Role.ADMIN, true), profileController.removeProfile);
router.get('/profile-roles', hasRole(Role.ADMIN, true), profileController.getRoles);
router.post('/profile-assign', hasRole(Role.ADMIN, true), profileController.assignRole);

module.exports = router;
