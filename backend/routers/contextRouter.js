const contextController = require('../controllers/contextController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.get('/context', hasRole(Role.USER, false), contextController.getCurrentContext);
router.get('/context-list', hasRole(Role.USER, true), contextController.getContextList);
router.put('/context', hasRole(Role.USER, true), contextController.changeCurrentContext);

module.exports = router;
