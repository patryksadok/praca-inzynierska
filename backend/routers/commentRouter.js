const commentController = require('../controllers/commentController');
const express = require('express');
const router = express.Router();
const hasRole = require('../middleware/hasRole');
const Role = require('../config/roles');

router.post('/comment-case/:id', hasRole(Role.USER, true), commentController.addComment);

module.exports = router;