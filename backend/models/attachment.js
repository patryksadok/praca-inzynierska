'use strict';

const cases = require('./cases.js');
const profiles = require('./profile.js');

module.exports = (sequelize, DataTypes) => {
    const attachment = sequelize.define('attachments', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        description: {
            type: DataTypes.STRING
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        confirmed: {
            type: DataTypes.BOOLEAN
        },
        caseId: {
            type: DataTypes.INTEGER,
            name: 'caseId',
            field: 'case_id',
            references: {
                model: cases,
                key: 'id',
            }
        },
        authorId: {
            type: DataTypes.INTEGER,
            name: 'authorId',
            field: 'author_id',
            references: {
                model: profiles,
                key: 'id'
            }
        }
    }, {
        underscored: true,
    });

    attachment.associate = models => {
        cases.belongsTo(models.attachments, {
            foreignKey: 'clientId'
        });
    };

    attachment.associate = models => {
    };

    return attachment;
};
