'use strict';

const type = require('./type.js');
const profile = require('./profile.js');
const verdicts = require('./verdicts.js');
module.exports = (sequelize, DataTypes) => {
    const cases = sequelize.define('cases', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        closedAt: {
            type: DataTypes.DATE,
            name: 'closedAt',
            field: 'closed_at'
        },
        awaiting: {
            type: DataTypes.BOOLEAN
        },
        description: {
            type: DataTypes.STRING
        },
        officialId: {
            allowNul: false,
            type: DataTypes.INTEGER,
            name: 'officialId',
            field:'official_id',
            references: {
                model: profile,
                key: 'id'
            }
        },
        clientId: {
            type: DataTypes.INTEGER,
            name: 'clientId',
            field: 'client_id',
            references: {
                model: profile,
                key: 'id',
            }
        },
        verdictId: {
            allowNull: true,
            type: DataTypes.INTEGER,
            name: 'verdictId',
            field: 'verdict_id',
            references: {
                model: verdicts,
                key: 'id',
            }
        }
    }, {
        underscored: true,
    });

    cases.associate = models => {
        cases.belongsTo(models.profiles, {
            foreignKey: 'clientId'
        });
    };

    cases.associate = models => {
        cases.belongsTo(models.profiles, {
            foreignKey: 'officialId'
        });
    };


    return cases;
};
