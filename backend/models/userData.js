'use strict';

const address = require('./address.js');
module.exports = (sequelize, DataTypes) => {
    const userdata = sequelize.define('userdata', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        phoneNumber: {
            type: DataTypes.INTEGER,
            name:'phoneNumber',
            field:'phone_number'
        },
        registeredAddressId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'registeredAddressId',
            field: 'registered_address_id',
            references: {
                model: address,
                key: 'id',
            }
        },
        correspondenceAddressId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'correspondenceAddressId',
            field: 'correspondence_address_id',
            references: {
                model: address,
                key: 'id',
            }
        },
        firstName: {
            type: DataTypes.STRING,
            name: 'firstName',
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.STRING,
            name: 'lastName',
            field: 'last_name'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {
        underscored: true,
    });

    userdata.associate = models => {
    };

    return userdata;
};
