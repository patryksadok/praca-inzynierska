'use strict';

module.exports = (sequelize, DataTypes) => {
    const type = sequelize.define('types', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        type: {
            type: DataTypes.STRING
        }
    }, {
        underscored: true,
    });

    type.associate = models => {
    };

    return type;
};
