'use strict';

module.exports = (sequelize, DataTypes) => {
    const userdata = require('./userData.js');
    const user = sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        email: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        userDataId: {
            type: DataTypes.INTEGER,
            name: 'userDataId',
            field: 'user_data_id',
            references: {
                model: userdata,
                key: 'id',
            }
        },
        activeProfile: {
            type: DataTypes.INTEGER,
            name: 'activeProfile',
            field: 'active_profile',
        },
        completed: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        underscored: true
    });

    user.associate = models => {
        user.hasMany(models.profiles, {
            foreignKey: 'user_id',
            as: 'userId',
        });
    };

    return user;
};
