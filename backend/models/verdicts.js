'use strict';

module.exports = (sequelize, DataTypes) => {
    const verdict = sequelize.define('verdict', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        isConfirmed: {
            type: DataTypes.BOOLEAN,
            name:'isConfirmed',
            field:'is_confirmed'
        },
        validationTime: {
            type: DataTypes.DATE,
            name: 'validationTime',
            field: 'validation_time'
        },
        description: {
            type: DataTypes.STRING
        }
    }, {
        underscored: true,
    });

    verdict.associate = models => {
    };

    return verdict;
};
