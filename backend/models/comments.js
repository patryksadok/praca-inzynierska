'use strict';

const cases = require('./cases.js');
const profile = require('./profile.js');
module.exports = (sequelize, DataTypes) => {
    const comment = sequelize.define('comments', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        value: {
            type: DataTypes.STRING
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        },
        caseId: {
            type: DataTypes.INTEGER,
            name: 'caseId',
            field: 'case_id',
            references: {
                model: cases,
                key: 'id',
            }
        },
        authorId: {
            type: DataTypes.INTEGER,
            name: 'authord',
            field: 'author_id',
            references: {
                model: profile,
                key: 'id'
            }
        }
    }, {
        underscored: true,
    });

    comment.associate = models => {
        cases.belongsTo(models.comment, {
            foreignKey: 'clientId'
        });
    };

    comment.associate = models => {
    };

    return comment;
};
