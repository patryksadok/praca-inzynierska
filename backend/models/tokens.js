'use strict';

const users = require('./users');
module.exports = (sequelize, DataTypes) => {
    const token = sequelize.define('tokens', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        refreshToken: {
            type: DataTypes.STRING,
            name: 'refreshToken',
            field: 'refresh_token',
        },
        accessToken: {
            type: DataTypes.STRING,
            name: 'accessToken',
            field: 'access_token',
        },
        userId: {
            allowNull: false,
            type: DataTypes.INTEGER,
            name: 'userId',
            field: 'user_id',
            references: {
                model: users,
                key: 'id',
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {});
    token.associate = models => {
    };
    return token;
};
