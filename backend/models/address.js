'use strict';

module.exports = (sequelize, DataTypes) => {
    const address = sequelize.define('addresses', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        country: {
            type: DataTypes.STRING
        },
        state: {
            type: DataTypes.STRING
        },
        city: {
            type: DataTypes.STRING
        },
        street: {
            type: DataTypes.STRING
        },
        homeNumber: {
            type: DataTypes.STRING,
            name: 'homeNumber',
            field: 'home_number'
        },
        flatNumber: {
            type: DataTypes.STRING,
            name: 'flatNumber',
            field: 'flat_number'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {
        underscored: true,
    });

    address.associate = models => {
    };

    return address;
};
