'use strict';

const roles = require('./roles.js');
const section = require('./section.js');
const users = require('./users.js');
const cases = require('./cases.js');

module.exports = (sequelize, DataTypes) => {
    const profile = sequelize.define('profiles', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        roleId: {
            type: DataTypes.INTEGER,
            name: 'roleId',
            field: 'role_id',
            references: {
                model: roles,
                key: 'id',
            }
        },
        sectionId: {
            type: DataTypes.INTEGER,
            name: 'sectionId',
            field: 'section_id',
            references: {
                model: section,
                key: 'id',
            }
        },
        userId: {
            type: DataTypes.INTEGER,
            name: 'userId',
            field: 'user_id',
            references: {
                model: users,
                key: 'id',
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            name: 'createdAt',
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            name: 'updatedAt',
            field: 'updated_at'
        }
    }, {
        underscored: true
    });

    profile.associate = models => {
        profile.belongsTo(models.users, {
            foreignKey: 'userId'
        });
    };

    profile.associate = models => {
        profile.hasMany(models.cases, {
            foreignKey: 'official_id',
            as: 'officialId',
        });
    };

    profile.associate = models => {
        profile.hasMany(models.cases, {
            foreignKey: 'client_id',
            as: 'clientId',
        });
    };

    return profile;
};
