const gulp = require("gulp");
const pm2 = require("pm2");
const dotenv = require("dotenv");
dotenv.config({ path: ".env" });

gulp.task("serve", () => {
  pm2.connect(
    false,
    () => {
      pm2.start(
        {
          name: "Backend server",
          script: "app.js",
          watch: true,
          ignore_watch: ["node_modules"],
          env: {
            NODE_ENV: process.env.NODE_ENV || "development"
          }
        },
        () => {
          pm2.streamLogs("all", 0);
        }
      );
    }
  );
});

gulp.task("default", ["serve"]);
