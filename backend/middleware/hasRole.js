const userRepository = require('../repositories/userRepository');
const jwt = require('jsonwebtoken');
const Role = require('../config/roles');
const TokenExpiredError = require('../errors/tokenExpiredError');
const ProfileNotCompleteError = require('../errors/profileNotCompleteError');
const NotEnoughPermissionsError = require('../errors/notEnoughPermissions');
const profileManager = require('../managers/profileManager');

const isComplete = async (userId) => {
    try {
        const foundUser = await userRepository.getUserById(userId);
        const isConfirmed = foundUser.dataValues.completed;
        return isConfirmed;
    } catch (error) {
        console.log(error);
    }
};

const getToken = async (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }
};

const hasRole = (role, checkComplete = false) => {
    return hasRole[role, checkComplete] || (hasRole[role, checkComplete] = async (req, res, next) => {
        try {
            if (!role) {
                role = Role.DEFAULT
            }
            const token = await getToken(req);

            const decoded = await jwt.verify(token, 'secret', (err, decoded) => {
                if (err) {
                    throw new TokenExpiredError();
                }
                return decoded;
            });

            const userId = await decoded.data.userId;

            if (checkComplete) {
                const completed = await isComplete(decoded.data.userId); 
                if (!completed) { 
                    throw new ProfileNotCompleteError();
                }
            }
            if (role === Role.DEFAULT) {
                return next();
            }
            const roleOfUser = await profileManager.getActiveProfileRole(userId);
            if (role !== roleOfUser) {
                //throw new NotEnoughPermissionsError();
            }

            return next();
        } catch (error) {
            console.log(error);
            return res.status(error.status).json({
                message: error.message,
                type: error.type,
            });
        }
    });
};

module.exports = hasRole;
